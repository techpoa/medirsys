// CONTACT MAP

var PageContact = function() {

	var _init = function() {

		var mapbg = new GMaps({
			div: '#gmapbg',
			lat: -1.273226,
			lng: 36.822780,
			scrollwheel: false,
		});

		mapbg.addMarker({
			lat: -1.273226,
			lng: 36.822780,
			title: 'Our Location',
			infoWindow: {
				content: '<h3>Medir Systems Inc.</h3><p>Main Office: Suraj Plaza(Opposite Jamhuri School)</p>'
			}
		});
	}

    return {
        //main function to initiate the module
        init: function() {

            _init();
        }
    };
}();

$(document).ready(function() {
    PageContact.init();
});