<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 4/29/2017
 * Time: 3:37 AM
 */
        ?>

        <!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title> Medir Systems </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta name="keywords" content="Survey Equipment, Civil Engineering Equipment, Total Station, Leveling Staff, Handheld GPS, GNSS RTK GPS, Automatic and Digital Level, Smart Station, Garmin
    "/>
    <meta itemprop="description" content="MEDIR SYSTEMS IS THE LEADING PROVIDER OF PRECISION MEASURING INSTRUMENTS,  SURVEYING INSTRUMENTS, LASER INSTRUMENTS, GIS GPS EQUIPMENTS AND OTHER HIGH PRECISION SYSTEMS. COMPANY NAME’ DIVERSE PRODUCT LINE PROVIDES COMPLETE MEASUREMENT SOLUTIONS FOR SURVEYING, MAPPING AND GIS, INDUSTRIAL MEASUREMENT AND CONSTRUCTION APPLICATIONS. RTK GPS, Total Stations, Theodolites and Digital Levels">
    <meta content="Devinvent Technologies" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700&amp;subset=all' rel='stylesheet' type='text/css'>
    <link href="{{asset('assets/plugins/socicon/socicon.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/bootstrap-social/bootstrap-social.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/animate/animate.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{asset('backend/plugins/datepicker/datepicker3.css')}}">
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN: BASE PLUGINS  -->
    <link href="{{asset('assets/plugins/cubeportfolio/css/cubeportfolio.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/owl-carousel/assets/owl.carousel.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/fancybox/jquery.fancybox.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/slider-for-bootstrap/css/slider.css')}}" rel="stylesheet" type="text/css" />
    <!-- END: BASE PLUGINS -->
    <!-- BEGIN THEME STYLES -->
    <link href="{{asset('assets/base/css/plugins.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/base/css/components.css')}}" id="style_components" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/base/css/themes/default.css')}}" rel="stylesheet" id="style_theme" type="text/css" />
    <link href="{{asset('assets/base/css/custom.css')}}" rel="stylesheet" type="text/css" />
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}" /> 
    </head>


<!-- BEGIN: CORE PLUGINS -->
<script src="{{asset('assets/plugins/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/jquery-migrate.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/jquery.easing.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/reveal-animate/wow.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/base/js/scripts/reveal-animate/reveal-animate.js')}}" type="text/javascript"></script>

<body class="c-layout-header-fixed c-layout-header-mobile-fixed c-layout-header-topbar c-layout-header-topbar-collapse">


<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

    @include('partials.header')

    @include('partials.authentication')

    @yield('content')
<!-- END: PAGE CONTAINER -->
    @include('partials.footer')
<!-- BEGIN: LAYOUT/BASE/BOTTOM -->

<!-- END: CORE PLUGINS -->
<!-- BEGIN: LAYOUT PLUGINS -->
<script src="{{asset('assets/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/owl-carousel/owl.carousel.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/counterup/jquery.counterup.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/counterup/jquery.waypoints.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/fancybox/jquery.fancybox.pack.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/smooth-scroll/jquery.smooth-scroll.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/slider-for-bootstrap/js/bootstrap-slider.js')}}" type="text/javascript"></script>

<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('backend/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{asset('backend/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<!-- END: LAYOUT PLUGINS -->
<!-- BEGIN: THEME SCRIPTS -->
<script src="{{asset('assets/base/js/components.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/base/js/components-shop.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/base/js/app.js')}}" type="text/javascript"></script>

    <!-- BEGIN: PAGE SCRIPTS -->
    {{--<script src="//maps.google.com/maps/api/js?sensor=true" type="text/javascript"></script>--}}
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8SyyT4D-TMLy-UpymLHVxUcX67ATxFHQ"></script>
    <script src="{{asset('assets/plugins/gmaps/gmaps.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/base/js/scripts/pages/contact.js')}}" type="text/javascript"></script>
    <!-- END: PAGE SCRIPTS -->
<script>
    $(document).ready(function()
    {
        App.init(); // init core
    });
</script>
<!-- END: THEME SCRIPTS -->

</body>


</html>
