@extends('layouts.index')
    @section('content')

        <!-- BEGIN: PAGE CONTAINER -->
        <div class="c-layout-page">
            <!-- BEGIN: PAGE CONTENT -->
            <!-- BEGIN: LAYOUT/SLIDERS/REVO-SLIDER-9 -->
            <section class="c-layout-revo-slider c-layout-revo-slider-9" dir="ltr">
                <div class="tp-banner-container tp-fullscreen tp-fullscreen-mobile">
                    <div class="tp-banner rev_slider" data-version="5.0">
                        <ul>
                            <!--BEGIN: SLIDE #1 -->
                            <li data-transition="fade" data-slotamount="1" data-masterspeed="1000">
                                <img alt="" src="{{asset('assets/base/img/content/slider/001.jpg')}}" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
                                <!--BEGIN: MAIN TITLE -->
                                <div class="tp-caption customin customout tp-resizeme" data-x="center" data-y="center" data-hoffset="[-50,-50, -50, -10]" data-voffset="-80" data-speed="500" data-start="1000" data-transform_in="x:0;y:0;z:0;rotationX:0.5;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;:s:500;e:Back.easeInOut;"
                                     data-transform_out="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;s:600;e:Back.easeInOut;" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1"
                                     data-endspeed="600">
                                    <h3 class="c-main-title c-font-44 c-font-bold c-font-uppercase c-font-white "> All About
                                        <br> Precision Instruments</h3>
                                </div>
                                <!--END -->
                                <!--BEGIN: ACTION BUTTON -->
                                <div class="tp-caption randomrotateout tp-resizeme" data-x="center" data-y="center" data-hoffset="[-70,-370, -10, -10]" data-voffset="40" data-speed="500" data-start="2000" data-transform_in="x:0;y:0;z:0;rotationX:0.5;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;s:500;e:Back.easeInOut;"
                                     data-transform_out="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;s:600;e:Back.easeInOut;">
                                    <a href="{{url('hire')}}" class="c-action-btn btn btn-xlg c-btn-square c-btn-bold c-btn-white c-btn-uppercase">Explore</a>
                                </div>
                                <!--END -->
                            </li>
                            <!--END -->
                            <!--BEGIN: SLIDE #2 -->
                            <li data-transition="fade" data-slotamount="1" data-masterspeed="1000">
                                <img alt="" src="{{asset('assets/base/img/content/slider/002.jpg')}}" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
                                <!--BEGIN: MAIN TITLE -->
                                <div class="tp-caption customin customout tp-resizeme" data-x="center" data-y="center" data-hoffset="[-350,-350, -50, -50]" data-voffset="-180" data-speed="500" data-start="1000" data-transform_in="x:0;y:0;z:0;rotationX:0.5;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;s:500;e:Back.easeInOut;"
                                     data-transform_out="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;s:600;e:Back.easeInOut;" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1"
                                     data-endspeed="600">
                                    <h3 class="c-main-title c-font-44 c-font-bold c-font-uppercase c-font-white "> Medir Systems 2017 </h3>
                                </div>
                                <!--END -->
                                <!--BEGIN: SUB TITLE -->
                                <div class="tp-caption customin customout tp-resizeme" data-x="center" data-y="center" data-hoffset="[-345,-345, -185, -145]" data-voffset="-70" data-speed="500" data-start="1500" data-transform_in="x:0;y:0;z:0;rotationX:0.5;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;s:500;e:Back.easeInOut;"
                                     data-transform_out="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;s:600;e:Back.easeInOut;" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1"
                                     data-endspeed="600">
                                    <p class="c-sub-title c-font-30 c-font-white c-font-bold">
                                        We are official partners with
                                        <br>  RUIDE in kenya as the resellers
                                        <br> of their verified instruments.
                                        <br>
                                    </p>

                                </div>
                                <!--END -->
                                <!--BEGIN: ACTION BUTTON -->
                                <div class="tp-caption randomrotateout tp-resizeme" data-x="center" data-y="center" data-hoffset="[-330,-370, -270, -270]" data-voffset="45" data-speed="500" data-start="2000" data-transform_in="x:0;y:0;z:0;rotationX:0.5;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;s:500;e:Back.easeInOut;"
                                     data-transform_out="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;s:600;e:Back.easeInOut;">
                                    <a href="{{url('product')}}" class="c-action-btn btn btn-xlg c-btn-square c-btn-bold c-theme-btn c-btn-uppercase">Explore</a>
                                </div>
                                <!--END -->
                            </li>
                            <!--END -->
                            <!--BEGIN: SLIDE #2 -->
                            <li data-transition="fade" data-slotamount="1" data-masterspeed="1000">
                                <img alt="" src="{{asset('assets/base/img/content/slider/003.jpg')}}" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
                                <!--BEGIN: MAIN TITLE -->
                                <div class="tp-caption customin customout tp-resizeme" data-x="center" data-y="center" data-hoffset="[350,350, 0, 0]" data-voffset="-60" data-speed="500" data-start="1000" data-transform_in="x:0;y:0;z:0;rotationX:0.5;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;s:500;e:Back.easeInOut;"
                                     data-transform_out="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;s:600;e:Back.easeInOut;" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1"
                                     data-endspeed="600">
                                    <h3 class="c-main-title c-font-44 c-font-bold c-font-uppercase c-font-black"> Medir Systems 2017 </h3>
                                </div>
                                <!--END -->
                                <!--BEGIN: SUB TITLE -->
                                <div class="tp-caption customin customout tp-resizeme" data-x="center" data-y="center" data-hoffset="[370,370, 30, 10]" data-voffset="30" data-speed="500" data-start="1500" data-transform_in="x:0;y:0;z:0;rotationX:0.5;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;s:500;e:Back.easeInOut;"
                                     data-transform_out="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;s:600;e:Back.easeInOut;" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1"
                                     data-endspeed="600">
                                    <p class="c-main-title-1 c-font-30 c-center c-font-uppercase c-font-bold c-font-black">We offer the best in services
                                        <br> when it comes to GIS
                                        <br> and surveying instruments
                                        <br></p>
                                </div>
                                <!--END -->
                                <!--BEGIN: ACTION BUTTON -->
                                <div class="tp-caption randomrotateout tp-resizeme" data-x="center" data-y="center" data-hoffset="[390, 390, 30, 10]" data-voffset="155" data-speed="500" data-start="2000" data-transform_in="x:0;y:0;z:0;rotationX:0.5;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;s:500;e:Back.easeInOut;"
                                     data-transform_out="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;s:600;e:Back.easeInOut;">
                                    <a href="{{url('product')}}" class="c-action-btn btn btn-xlg c-btn-square c-btn-bold  c-theme-btn c-bg- c-btn-uppercase">Explore</a>
                                </div>
                                <!--END -->
                            </li>
                            <!--END -->
                            <!--BEGIN: SLIDE #2 -->
                            <li data-transition="fade" data-slotamount="1" data-masterspeed="1000">
                                <img alt="" src="{{asset('assets/base/img/content/slider/004.jpg')}}" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
                                <!--BEGIN: MAIN TITLE -->
                            </li>
                            <!--END -->
                            <!--BEGIN: SLIDE #2 -->
                            <li data-transition="fade" data-slotamount="1" data-masterspeed="1000">
                                <img alt="" src="{{asset('assets/base/img/content/slider/005.jpg')}}" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
                                <!--BEGIN: MAIN TITLE -->
                            </li>
                            <!--END -->
                            <!--BEGIN: SLIDE #2 -->
                            <li data-transition="fade" data-slotamount="1" data-masterspeed="1000">
                                <img alt="" src="{{asset('assets/base/img/content/slider/006.jpg')}}" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
                                <!--BEGIN: MAIN TITLE -->
                                <div class="tp-caption customin customout tp-resizeme" data-x="center" data-y="center" data-hoffset="[350,350, 0, 0]" data-voffset="120" data-speed="500" data-start="1000" data-transform_in="x:0;y:0;z:0;rotationX:0.5;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;s:500;e:Back.easeInOut;"
                                     data-transform_out="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;s:600;e:Back.easeInOut;" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1"
                                     data-endspeed="600">
                                    <h3 class="c-main-title c-font-44 c-font-bold c-font-uppercase c-font-white "> Medir Systems 2017 </h3>
                                </div>
                                <!--END -->
                                <!--BEGIN: SUB TITLE -->
                                <div class="tp-caption customin customout tp-resizeme" data-x="center" data-y="center" data-hoffset="[445,445, -145, -145]" data-voffset="30" data-speed="500" data-start="1500" data-transform_in="x:0;y:0;z:0;rotationX:0.5;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;s:500;e:Back.easeInOut;"
                                     data-transform_out="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;s:600;e:Back.easeInOut;" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1"
                                     data-endspeed="600">
                                    <p class="c-sub-title c-font-30 c-font-white ">
                                        Our Services Range from Leasing
                                        <br> and selling from surveying to
                                        <br> mapping plus GIS gadgets and unbeatable prices
                                        <br>
                                    </p>
                                </div>
                                <!--END -->
                            </li>
                            <!--END -->
                        </ul>
                    </div>
                </div>
            </section>
            <!-- END: LAYOUT/SLIDERS/REVO-SLIDER-9 -->
            <!-- BEGIN: CONTENT/SHOPS/SHOP-1-1 -->
            <div class="c-content-box c-size-md">
                <div class="container">
                    <div class="c-content-tab-5 c-bs-grid-reset-space c-theme">
                        <!-- Nav tabs -->
                        <ul class="nav nav-pills c-nav-tab c-arrow" role="tablist">
                            @foreach($categories as $key => $category)
                                @if($key == 0)
                                    <li role="presentation" class="active">
                                        <a class="c-font-uppercase" href="#{!! $category->id !!}" aria-controls="{!! $category->id !!}" role="tab" data-toggle="tab">{!! $category -> name !!}</a>
                                    </li>
                                @else
                                    <li role="presentation">
                                        <a class="c-font-uppercase" href="#{!! $category->id !!}" aria-controls="{!! $category->id !!}" role="tab" data-toggle="tab">{!! $category -> name !!}</a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            @foreach($categories as $key => $category)
                                @if($key == 0)
                                    <div role="tabpanel" class="tab-pane fade in active" id="{!! $category->id !!}">
                                        <div class="row">
                                            @foreach(\App\Products::where('category_id','=',$category -> id) ->limit(4)->get() as  $product)
                                            <div class="col-sm-3">
                                                <div class="c-content c-content-overlay">
                                                    <div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url({{asset($product->image)}});"></div>
                                                    <div class="c-overlay-border"></div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    @else
                                    <div role="tabpanel" class="tab-pane fade in" id="{!! $category->id !!}">
                                        <div class="row">
                                            @foreach(\App\Products::where('category_id','=',$category -> id) ->limit(4)->get() as  $product)
                                                <div class="col-sm-3">
                                                    <div class="c-content c-content-overlay">
                                                        <div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url({{asset($product->image)}});"></div>
                                                        <div class="c-overlay-border"></div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <!-- END: CONTENT/SHOPS/SHOP-1-1 -->
            <!-- BEGIN: CONTENT/BARS/BAR-3 -->
            <div class="c-content-box c-size-md c-bg-dark">
                <div class="container">
                    <div class="c-content-bar-3">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="c-content-title-1">
                                    <h3 class="c-font-uppercase c-font-bold">Welcome to Our Website ! Medir Systems Ltd.</h3>
                                    <p class="c-font-uppercase c-font-grey">
                                        Medir Systems is the leading provider of precision measuring instruments. The company is
                                        promoted by pioneers in the field of Surveying instruments, Laser instruments, GIS GPS equipments and other high
                                        precision systems. Company Name’ diverse product line provides complete measurement solutions for surveying, mapping
                                        and GIS, industrial measurement and construction applications. Our products have been used to complete major projects.

                                    </p>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="c-content-v-center">
                                    <div class="c-wrapper">
                                        <div class="c-body">
                                            <a href="{{url('products')}}" type="button" class="btn btn-md c-btn-square c-btn-border-2x c-theme-btn c-btn-uppercase c-btn-bold">View Instruments</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END: CONTENT/BARS/BAR-3 -->
            <!-- BEGIN: CONTENT/SHOPS/SHOP-2-1 -->
            <div class="c-content-box c-size-md c-overflow-hide c-bs-grid-small-space c-bg-grey-1">
                <div class="container">
                    <div class="c-content-title-4">
                        <h3 class="c-font-uppercase c-center c-font-bold c-line-strike">
                            <span class="c-bg-grey-1">Most Popular</span>
                        </h3>
                    </div>
                    <div class="row">
                        @foreach($items as $item)
                        <div class="col-md-4 col-sm-6 c-margin-b-20">
                            <div class="c-content-product-2 c-bg-white">
                                <div class="c-content-overlay">
                                    <div class="c-overlay-wrapper">
                                        <div class="c-overlay-content">
                                            <a href="{{url('product/details/'.$item->id)}}" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
                                        </div>
                                    </div>
                                    <div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 300px; background-image: url({{asset($item->image)}});"></div>
                                </div>
                                <div class="c-info">
                                    <p class="c-title c-font-18 c-font-slim">{{ $item->name }}</p>
                                </div>
                                <div class="btn-group btn-group-justified" role="group">
                                    <div class="btn-group c-border-left c-border-top" role="group">
                                        <form action="/cart" method="POST">
                                            {!! csrf_field() !!}
                                            <input type="hidden" name="id" value="{{ $item->id }}">
                                            <input type="hidden" name="name" value="{{ $item->name }}">
                                            <input type="hidden" name="price" value="{{ $item->price }}">
                                            <input type="submit" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product" value="Add to Cart">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <!-- END: CONTENT/SHOPS/SHOP-2-1 -->
            <!-- BEGIN: CONTENT/SHOPS/SHOP-3-2 -->
            <div class="c-content-box c-size-lg c-no-padding c-overflow-hide c-bg-white">
                <div class="c-content-product-3 c-bs-grid-reset-space">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="c-wrapper c-theme-bg" style="height: 450px;">
                                <div class="c-content c-border c-border-padding c-border-padding-left">
                                    <div class="c-info">
                                        <h3 class="c-title c-font-25 c-font-white c-font-uppercase c-font-bold">{{$featured -> name}}</h3>
                                        <p class="c-description c-font-17 c-font-white">
                                            {!! $featured -> description !!}
                                        </p>
                                    </div>
                                    <div class="btn-group btn-group-justified" style="margin-top: 150px;" role="group">
                                        <div class="btn-group c-border-left c-border-top" role="group">
                                            <a href="{{url('product/details/'.$featured -> id)}}" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product"> Check Details</a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="c-content-overlay">
                                <div class="c-overlay-wrapper">
                                    <div class="c-overlay-content">
                                        <a href="{{url('product/details/'.$featured -> id)}}" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
                                    </div>
                                </div>
                                <div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 450px; background-image: url({{asset($featured->image)}});"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

                @include('partials.offers')
            <!-- END: CONTENT/SHOPS/SHOP-3-2 -->
            <!-- BEGIN: CONTENT/clients -->
                @include('partials.clients')
            <!-- END: CONTENT/clients -->
            <!-- END: PAGE CONTENT -->
        </div>
        <!-- END: PAGE CONTAINER -->
     @endsection
