<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 4/29/2017
 * Time: 3:37 AM
 */?>

<!-- BEGIN: LAYOUT/FOOTERS/FOOTER-5 -->
<a name="footer"></a>
<footer class="c-layout-footer c-layout-footer-3 c-bg-dark">
    <div class="c-prefooter">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <form action="{{url('subscriptions')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="input-group input-group-lg c-square">
                            <input type="email" name="email" class="c-input form-control c-square c-theme" placeholder="Your Email Here" required>
                            <span class="input-group-btn">
                                    <button class="btn c-theme-btn c-theme-border c-btn-square c-btn-uppercase c-font-16" type="submit">Subscribe</button>
                                </span>
                        </div>
                    </form>
                    <br>
                    <div class="c-container c-first">
                        <div class="c-content-title-1">
                            <h3 class="c-font-uppercase c-font-bold c-font-white">MEDIR
                                <span class="c-theme-font">SYSTEMS</span>
                            </h3>
                            <div class="c-line-left hide"></div>
                            <p class="c-text c-font-grey">
                                P.O BOX  45562-00100,
                                NAIROBI.
                            </p>
                        </div>
                        <ul class="c-links">
                            <li>
                                <a href="{{url('/')}}" class="c-font-grey">Home</a>
                            </li>
                            <li>
                                <a href="{{url('about')}}" class="c-font-grey">About</a>
                            </li>
                            <li>
                                <a href="{{url('contacts')}}" class="c-font-grey">Contact</a>
                            </li>
                            <li>
                                <a href="{{url('product')}}" class="c-font-grey">Producs</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="c-container">
                        <div class="c-content-title-1">
                            <h3 class="c-font-uppercase c-font-bold c-font-white">Latest Products</h3>
                            <div class="c-line-left hide"></div>
                        </div>
                        <div class="c-blog">
                            <div class="c-post">
                                <h4 class="c-post-title">
                                    <a href="#" class="c-font-grey">V60 GNSS RTK SYSTEM</a>
                                </h4>
                                <p class="c-text c-font-grey">
                                    The V60 is a more compact-design and higher-performance GNSS RTK system
                                </p>
                            </div>
                            <div class="c-post c-last">
                                <h4 class="c-post-title">
                                    <a href="#" class="c-font-grey">Dedicated Support</a>
                                </h4>
                                <p class="c-text c-font-grey">
                                    The Magellan eXplorist 110 GPS receiver comes with essential navigation
                                    features ...
                                    .</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="c-container">
                        <div class="fb-page" data-href="https://www.facebook.com/medirsystems/"
                             data-tabs="timeline" data-height="300" data-small-header="false"
                             data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                            <blockquote cite="https://www.facebook.com/medirsystems/" class="fb-xfbml-parse-ignore">
                                <a href="https://www.facebook.com/medirsystems/">MEDIR Systems Limited</a>
                            </blockquote>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="c-container c-last">
                        <div class="c-content-title-1">
                            <h3 class="c-font-uppercase c-font-bold c-font-grey">Find us</h3>
                            <div class="c-line-left hide"></div>
                            <p class="c-font-grey">We are open monday to saturday at.</p>
                        </div>
                        <ul class="c-socials">
                            <li>
                                <a href="#">
                                    <i class="icon-social-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="icon-social-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="icon-social-youtube"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="icon-social-tumblr"></i>
                                </a>
                            </li>
                        </ul>
                        <ul class="c-address">
                            <li class="c-font-grey">
                                <i class="icon-pointer c-theme-font"></i>
                                Main Office: Suraj Plaza(Opposite Jamhuri School),
                                5th Floor , Suite 501
                                Limuru Road Parklands, (Opposite Jamhuri High School)
                            </li>
                            <li class="c-font-grey">
                                <i class="icon-call-end c-theme-font"></i> 0725 693 748 | 0722 605 682
                            </li>
                            <li class="c-font-grey">
                                <i class="icon-envelope c-theme-font"></i> info@medirsystems.com</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="c-postfooter">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 c-col text-center">
                    <p class="c-copyright c-font-grey">2017 &copy; Medir Systems
                        <span class="c-font-grey">All Rights Reserved.</span>
                         Designed and Developed
                                                <a href="http://www.devinvent.com/" target="_blank" class="c-font-grey"> Devinvent Technologies</span>

                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- END: LAYOUT/FOOTERS/FOOTER-5 -->
<!-- BEGIN: LAYOUT/FOOTERS/GO2TOP -->
<div class="c-layout-go2top">
    <i class="icon-arrow-up"></i>
</div>  
<!-- END: LAYOUT/FOOTERS/GO2TOP -->
