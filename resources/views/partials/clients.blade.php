<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 4/29/2017
 * Time: 4:07 AM
 */?>

<!-- BEGIN: CONTENT/SLIDERS/CLIENT-LOGOS-1 -->
<div class="c-content-box c-size-md c-bg-white">
    <div class="container">
        <!-- Begin: Testimonals 1 component -->
        <div class="c-content-client-logos-slider-1" data-slider="owl">
            <!-- Begin: Title 1 component -->
            <div class="c-content-title-1">
                <h3 class="c-center c-font-uppercase c-font-bold">Our Partners</h3>
                <div class="c-line-center c-theme-bg"></div>
            </div>
            <!-- End-->
            <!-- Begin: Owlcarousel -->
            <div class="owl-carousel owl-theme c-theme c-owl-nav-center" data-items="5" data-desktop-items="4" data-desktop-small-items="3" data-tablet-items="3" data-mobile-small-items="1" data-auto-play="false" data-rtl="false" data-slide-speed="5000"
                 data-auto-play-hover-pause="true">
                <div class="item">
                    <a href="#">
                        <img src="{{asset('assets/base/img/content/client-logos/client1.jpg')}}" alt="" />
                    </a>
                </div>
                <div class="item">
                    <a href="#">
                        <img src="{{asset('assets/base/img/content/client-logos/client2.jpg')}}" alt="" />
                    </a>
                </div>
                <div class="item">
                    <a href="#">
                        <img src="{{asset('assets/base/img/content/client-logos/client3.jpg')}}" alt="" />
                    </a>
                </div>
                <div class="item">
                    <a href="#">
                        <img src="{{asset('assets/base/img/content/client-logos/client4.jpg')}}" alt="" />
                    </a>
                </div>
                <div class="item">
                    <a href="#">
                        <img src="{{asset('assets/base/img/content/client-logos/client5.jpg')}}" alt="" />
                    </a>
                </div>
                <div class="item">
                    <a href="#">
                        <img src="{{asset('assets/base/img/content/client-logos/client6.jpg')}}" alt="" />
                    </a>
                </div>
            </div>
            <!-- End-->
        </div>
        <!-- End-->
    </div>
</div>
<!-- END: CONTENT/SLIDERS/CLIENT-LOGOS-1 -->
