<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 4/29/2017
 * Time: 3:37 AM
 */?>

<!-- BEGIN: LAYOUT/HEADERS/HEADER-2 -->
<!-- BEGIN: HEADER 2 -->
<header class="c-layout-header c-layout-header-6" data-minimize-offset="80">
    <div class="c-topbar">
        <div class="container">
            <nav class="c-top-menu">
                <ul class="c-links c-theme-ul">
                    <li>
                        <a href="{{url('contacts')}}" class="c-font-uppercase c-font-bold">Help</a>
                    </li>
                    <li>
                        <a href="{{url('contacts')}}" class="c-font-uppercase c-font-bold">Contact</a>
                    </li>
                    <li class="c-divider"></li>
                    <li>
                        <a href="{{url('contacts')}}" class="c-font-uppercase c-font-bold c-font-dark">Support</a>
                    </li>
                    <li>
                        @if(Auth::check())
                            <a href="{{url('home')}}" class="btn c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">My Account </a>
                        @else
                            <a href="javascript:;" data-toggle="modal" data-target="#login-form" class="btn c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Sign In</a>
                        @endif
                    </li>
                </ul>
                <ul class="c-ext hide c-theme-ul">
                    <li class="c-lang dropdown c-last">
                        <a href="#">en</a>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li class="active">
                                <a href="#">English</a>
                            </li>
                            <li>
                                <a href="#">German</a>
                            </li>
                            <li>
                                <a href="#">Espaniol</a>
                            </li>
                            <li>
                                <a href="#">Portugise</a>
                            </li>
                        </ul>
                    </li>
                    <li class="c-search hide">
                        <!-- BEGIN: QUICK SEARCH -->
                        <form action="#">
                            <input type="text" name="query" placeholder="search..." value="" class="form-control" autocomplete="off">
                            <i class="fa fa-search"></i>
                        </form>
                        <!-- END: QUICK SEARCH -->
                    </li>
                </ul>
            </nav>
            <div class="c-brand">
                <a href="{{url("/")}}" class="c-logo">
                    <img src="{{asset('assets/base/img/layout/logos/logo-3.png')}}" alt="Medir Systems" class="c-desktop-logo">
                    <img src="{{asset('assets/base/img/layout/logos/logo-1.png')}}" alt="Medir Systems" class="c-desktop-logo-inverse">
                    <img src="{{asset('assets/base/img/layout/logos/logo-3.png')}}" alt="Medir Systems" class="c-mobile-logo"> </a>
                <ul class="c-icons c-theme-ul">
                    <li>
                        <a href="#">
                            <i class="icon-social-twitter"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="icon-social-facebook"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="icon-social-dribbble"></i>
                        </a>
                    </li>
                </ul>
                <button class="c-topbar-toggler" type="button">
                    <i class="fa fa-ellipsis-v"></i>
                </button>
                <button class="c-hor-nav-toggler" type="button" data-target=".c-mega-menu">
                    <span class="c-line"></span>
                    <span class="c-line"></span>
                    <span class="c-line"></span>
                </button>
                <button class="c-search-toggler" type="button">
                    <i class="fa fa-search"></i>
                </button>
            </div>
        </div>
    </div>
    <div class="c-navbar">
        <div class="container">
            <!-- BEGIN: BRAND -->
            <div class="c-navbar-wrapper clearfix">
                <!-- END: BRAND -->
                <!-- BEGIN: QUICK SEARCH -->
                <form class="c-quick-search" action="#">
                    <input type="text" name="query" placeholder="Type to search..." value="" class="form-control" autocomplete="off">
                    <span class="c-theme-link">&times;</span>
                </form>
                <!-- END: QUICK SEARCH -->
                <!-- BEGIN: HOR NAV -->
                <!-- BEGIN: LAYOUT/HEADERS/MEGA-MENU -->
                <!-- BEGIN: MEGA MENU -->
                <!-- Dropdown menu toggle on mobile: c-toggler class can be applied to the link arrow or link itself depending on toggle mode -->
                <nav class="c-mega-menu c-pull-right c-mega-menu-dark c-mega-menu-dark-mobile c-fonts-uppercase c-fonts-bold">
                    <ul class="nav navbar-nav c-theme-nav">
                        <li class="{!! Request::is('/') ? 'c-active' : '' !!}">
                            <a href="{{url('/')}}" class="c-link">Home
                                <span class="c-arrow c-toggler"></span>
                            </a>
                        </li>
                        <li class="{!! Request::is('about','about/*') ? 'c-active' : '' !!}">
                            <a href="{{url('about')}}" class="c-link">About Us
                            </a>
                        </li>
                        <li class="{!! Request::is('product','product/*') ? 'c-active' : '' !!}">
                            <a href="{{url('product')}}" class="c-link"> Products
                            </a>
                        </li>
                        <li class="{!! Request::is('hire','hire/*') ? 'c-active' : '' !!}">
                            <a href="{{url('hire')}}" class="c-link"> For Hire
                            </a>
                        </li>
                        <li class="{!! Request::is('services','services/*') ? 'c-active' : '' !!}">
                            <a href="{{url('services')}}" class="c-link"> Services
                            </a>
                        </li>
                        </li>
                        <li class="{!! Request::is('contacts','contacts/*') ? 'c-active' : '' !!}">
                            <a href="{{url('contacts')}}" class="c-link"> Contact Us
                            </a>
                        </li>
                        <li class="c-cart-toggler-wrapper">
                            <a href="#" class="c-btn-icon c-cart-toggler">
                                <i class="icon-handbag c-cart-icon"></i>
                                <span class="c-cart-number c-theme-bg">{{ sizeof($contents) }}</span>
                            </a>
                        </li>
                    </ul>
                </nav>
                <!-- END: MEGA MENU -->
                <!-- END: LAYOUT/HEADERS/MEGA-MENU -->
                <!-- END: HOR NAV -->
            </div>
            <!-- BEGIN: LAYOUT/HEADERS/QUICK-CART -->
            <!-- END: LAYOUT/HEADERS/QUICK-CART -->
        </div>
        <!-- BEGIN: CART MENU -->
        <div class="c-cart-menu">
{{--            @if (sizeof(Cart::content()) > 0)--}}

             @if(sizeof(Cart::instance('default')->content()) > 0)
            <div class="c-cart-menu-title">
                <p class="c-cart-menu-float-l c-font-sbold">{{ sizeof(Cart::instance('default')->content()) }} item(s)</p>
                <p class="c-cart-menu-float-r c-theme-font c-font-sbold">Ksh {{ Cart::instance('default')->subtotal() }}</p>
            </div>

                    <ul class="c-cart-menu-items">
                        @foreach ($contents as $item)
                            <li>
                                <div class="c-cart-menu-close">
                                    <form action="{{ url('cart', [$item->rowId]) }}" method="POST" class="side-by-side">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button type="submit" class="c-theme-link">×</button>
                                        {{--<input type="submit" class="btn btn-danger btn-sm" value="Remove">--}}
                                    </form>
                                </div>
                                <img src="{{asset(\App\Products::find($item->id)->thumbnail)}}" />
                                <div class="c-cart-menu-content">
                                    <p>1 x
                                        <span class="c-item-price c-theme-font">{{$item -> price}}</span>
                                    </p>
                                    <a href="{{url('product/details',$item->id)}}" class="c-item-name c-font-sbold">{{ $item->name }}</a>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                    <div class="c-cart-menu-footer">
                        <a href="{{url('cart')}}" class="btn btn-md c-btn c-btn-square c-btn-grey-3 c-font-white c-font-bold c-center c-font-uppercase">View Cart</a>
                        <a href="{{url('cart/checkout')}}" class="btn btn-md c-btn c-btn-square c-theme-btn c-font-white c-font-bold c-center c-font-uppercase">Checkout</a>
                    </div>
                @elseif(sizeof(Cart::instance('hire')->content()) > 0)

                    <div class="c-cart-menu-title">
                        <p class="c-cart-menu-float-l c-font-sbold">{{ sizeof(Cart::instance('hire')->content()) }} hire item(s)</p>
                        <p class="c-cart-menu-float-r c-theme-font c-font-sbold">Ksh {{ Cart::instance('hire')->subtotal() }}</p>
                    </div>
            <ul class="c-cart-menu-items">
                @foreach (Cart::instance('hire')->content() as $gadget)
                <li>
                    <div class="c-cart-menu-close">
                        <form action="{{ url('cart', [$gadget->rowId]) }}" method="POST" class="side-by-side">
                            {!! csrf_field() !!}
                            <input type="hidden" name="_method" value="DELETE">
                            <button type="submit" class="c-theme-link">×</button>
                            {{--<input type="submit" class="btn btn-danger btn-sm" value="Remove">--}}
                        </form>
                    </div>
                    <img src="{{asset(\App\Products::find($gadget->id)->thumbnail)}}" />
                    <div class="c-cart-menu-content">
                        <p>1 x
                            <span class="c-item-price c-theme-font">{{$gadget -> hire_price}}</span>
                        </p>
                        <a href="{{url('product/details',$gadget->id)}}" class="c-item-name c-font-sbold">{{ $gadget->name }}</a>
                    </div>
                </li>
                @endforeach
            </ul>
            <div class="c-cart-menu-footer">
                <a href="{{url('cart/hire')}}" class="btn btn-md c-btn c-btn-square c-btn-grey-3 c-font-white c-font-bold c-center c-font-uppercase">View Cart</a>
                <a href="{{url('cart/hirecheckout')}}" class="btn btn-md c-btn c-btn-square c-theme-btn c-font-white c-font-bold c-center c-font-uppercase">Checkout</a>
            </div>
            @else
                <div class="c-cart-menu-title">
                    <p class="c-cart-menu-float-l c-font-sbold">No Items in cart</p>
                    <a href="{{url('product')}}" class="c-cart-menu-float-r c-theme-font c-font-sbold"> Check Some</a>
                </div>
            @endif
        </div>
        <!-- END: CART MENU -->
    </div>
</header>
<!-- END: HEADER 2 -->
<!-- END: LAYOUT/HEADERS/HEADER-2 -->
