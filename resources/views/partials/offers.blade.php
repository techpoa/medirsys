<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 5/12/2017
 * Time: 2:36 PM
 */
?>

<!-- BEGIN: CONTENT/SHOPS/SHOP-2-2 -->
<div class="c-content-box c-size-md c-overflow-hide c-bs-grid-small-space">
    <div class="container">
        <div class="c-content-title-4">
            <h3 class="c-font-uppercase c-center c-font-bold c-line-strike">
                <span class="c-bg-white"> On Offer</span>
            </h3>
        </div>
        <div class="row">
            <div data-slider="owl">
                <div class="owl-carousel owl-theme c-theme owl-small-space c-owl-nav-center" data-rtl="false" data-items="4" data-slide-speed="8000">
                    @foreach($offers as $offer)
                    <div class="item">
                        <div class="c-content-product-2 c-bg-white c-border">
                            <div class="c-content-overlay">
                                <div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold">Sale</div>
                                <div class="c-overlay-wrapper">
                                    <div class="c-overlay-content">
                                        <a href="{{url("product/details/$offer->id")}}" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">View Details</a>
                                    </div>
                                </div>
                                <div class="c-bg-img-center-contain c-overlay-object" data-height="height" style="height: 270px; background-image: url({{ $offer->image }});"></div>
                            </div>
                            <div class="c-info">
                                <p class="c-title c-font-18 c-font-slim">{{ $offer-> name }}</p>
{{--                                 <p class="c-price c-font-16 c-font-slim">Ksh {{ $offer-> offer_price  }} &nbsp;
                                    <span class="c-font-16 c-font-line-through c-font-red">Ksh {{ $offer-> price }}</span>
                                </p> --}}
                            </div>
                            <div class="btn-group btn-group-justified" role="group">
                                <div class="btn-group c-border-left c-border-top" role="group">
                                    <form action="/cart" method="POST">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="id" value="{{ $offer->id }}">
                                        <input type="hidden" name="name" value="{{ $offer->name }}">
                                        @if($offer->on_offer = 1)
                                            <input type="hidden" name="price" value="{{ $offer->offer_price }}">
                                        @else
                                            <input type="hidden" name="price" value="{{ $offer->price }}">
                                        @endif
                                        <input type="submit" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product" value="Add to Cart">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                        @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: CONTENT/SHOPS/SHOP-2-2 -->
