<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 5/10/2017
 * Time: 8:09 PM
 */
?>
@extends('layouts.master')
@section('content')

<!-- BEGIN: PAGE CONTAINER -->
<div class="c-layout-page">
    <!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-3 -->
    <div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
        <div class="container">
            <div class="c-page-title c-pull-left">
                <h3 class="c-font-uppercase c-font-sbold">Our Services</h3>
            </div>
            <ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
                <li><a href="{{url('/')}}">Home </a></li>
                <li>/</li>
                <li class="c-state_active">Services</li>
            </ul>
        </div>
    </div>
    <!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-3 -->
    <!-- BEGIN: PAGE CONTENT -->
    <!-- BEGIN: CONTENT/PRODUCTS/PRODUCT-1 -->
    <div class="c-content-box c-size-md c-bg-white c-no-bottom-padding">
        <div class="container">
            <div class="c-content-product-1 c-opt-1">
                <div class="c-content-title-1">
                    <h3 class="c-center c-font-uppercase c-font-bold">Our Services</h3>
                    <div class="c-line-center"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: CONTENT/PRODUCTS/PRODUCT-1 -->
    <!-- BEGIN: CONTENT/MISC/SERVICES-1 -->
    <div class="c-content-box c-size-md c-bg-grey-1  ">
        <div class="container">
            <div class="c-content-feature-2-grid" data-auto-height="true" data-mode="base-height">
                <div class="row">
                    <div class="col-md-4 col-sm-6">
                        <div class="c-content-feature-2" data-height="height">
                            <div class="c-icon-wrapper">
                                <div class="c-content-line-icon c-theme c-icon-screen-chart"></div>
                            </div>
                            <h3 class="c-font-uppercase c-font-bold c-title">Sale of Equipments</h3>
                            <p>We sell equipments and we are the affirmed distributors</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="c-content-feature-2" data-height="height">
                            <div class="c-icon-wrapper">
                                <div class="c-content-line-icon c-theme c-icon-support"></div>
                            </div>
                            <h3 class="c-font-uppercase c-font-bold c-title">Hire of Equipments</h3>
                            <p>Some Gadgets are available for a limited period of time to final users</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="c-content-feature-2" data-height="height">
                            <div class="c-icon-wrapper">
                                <div class="c-content-line-icon c-theme c-icon-comment"></div>
                            </div>
                            <h3 class="c-font-uppercase c-font-bold c-title">Maintenance of Equipments</h3>
                            <p>We provide general maintenace of surveying equipment</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="c-content-feature-2" data-wow-delay1="2s" data-height="height">
                            <div class="c-icon-wrapper">
                                <div class="c-content-line-icon c-theme c-icon-bulb"></div>
                            </div>
                            <h3 class="c-font-uppercase c-font-bold c-title">Servicing of Equipments</h3>
                            <p>We do general servicing of surveying equipments</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="c-content-v-center c-theme-bg wow bounceInUp" data-wow-delay1="2s" data-height="height">
                            <div class="c-wrapper">
                                <div class="c-body c-padding-20 c-center">
                                    <h3 class="c-font-19 c-line-height-28 c-font-uppercase c-font-white c-font-bold"> Providing the best possible service to our customers without a breaking a sweat </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="c-content-feature-2" data-wow-delay1="2s" data-height="height">
                            <div class="c-icon-wrapper">
                                <div class="c-content-line-icon c-theme c-icon-globe"></div>
                            </div>
                            <h3 class="c-font-uppercase c-font-bold c-title">Repair of Equipments</h3>
                            <p> Fixing and Restoring of damaged/broken equipment </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: CONTENT/MISC/SERVICES-1 -->
    <!-- BEGIN: CONTENT/BARS/BAR-3 -->
    <div class="c-content-box c-size-md c-bg-dark">
        <div class="container">
            <div class="c-content-bar-3">
                <div class="row">
                    <div class="col-md-7">
                        <div class="c-content-title-1">
                            <h3 class="c-font-uppercase c-font-bold"> DEDICATED SUPPORT</h3>
                            <p class="c-font-uppercase">Medir Systems strives to become top-of-the-line to ensure that we provide the best experience for our customers</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-md-offset-2">
                        <div class="c-content-v-center" style="height: 90px;">
                            <div class="c-wrapper">
                                <div class="c-body">
                                    <a href="{{ url('contacts') }}" type="button" class="btn btn-md c-btn-square c-btn-border-2x c-theme-btn c-btn-uppercase c-btn-bold">Get Support</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: CONTENT/BARS/BAR-3 -->
@include('partials.clients')
    <!-- END: PAGE CONTENT -->
</div>
<!-- END: PAGE CONTAINER -->
@endsection