<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 4/29/2017
 * Time: 4:54 AM
 */
?>
@extends('layouts.master')
@section('content')
    <!-- BEGIN: PAGE CONTAINER -->
    <div class="c-layout-page">
        <!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
        <div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
            <div class="container">
                <div class="c-page-title c-pull-left">
                    <h3 class="c-font-uppercase c-font-sbold">For Hire</h3>
                </div>
                <ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
                    <li>
                        <a href="{{url('/')}}">Home </a>
                    </li>
                    <li>/</li>
                    <li class="c-state_active">For Hire</li>
                </ul>
            </div>
        </div>
        <!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
        <div class="container">
            <div class="c-layout-sidebar-menu c-theme ">
                <!-- BEGIN: LAYOUT/SIDEBARS/SHOP-SIDEBAR-MENU -->
                <div class="c-sidebar-menu-toggler">
                    <h3 class="c-title c-font-uppercase c-font-bold">Navigation</h3>
                    <a href="javascript:;" class="c-content-toggler" data-toggle="collapse" data-target="#sidebar-menu-1">
                        <span class="c-line"></span>
                        <span class="c-line"></span>
                        <span class="c-line"></span>
                    </a>
                </div>
                <ul class="c-sidebar-menu collapse " id="sidebar-menu-1">
                    <li class="c-dropdown c-active c-open">
                        <a href="#" class="c-toggler text-center"> Categories Filter </a>
                    </li>

                    @foreach($categories as $category)
                        <li class="c-dropdown c-active c-open">
                            <form action="#" method="POST">
                                {!! csrf_field() !!}
                                <input type="hidden" id="category_sort_id" name="category" value="{{ $category->id }}">

                                <input type="submit" id="category_sort" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-btn-border-1x c-font-grey-3 btn-block" value="{{ $category -> name }}">
                            </form>
                        </li>
                    @endforeach
                </ul>
                <!-- END: LAYOUT/SIDEBARS/SHOP-SIDEBAR-MENU -->
            </div>
            <div class="c-layout-sidebar-content ">
                <!-- BEGIN: PAGE CONTENT -->
                <!-- BEGIN: CONTENT/SHOPS/SHOP-RESULT-FILTER-1 -->
                <!-- END: CONTENT/SHOPS/SHOP-RESULT-FILTER-1 -->
                <div class="c-margin-t-20"></div>
                <!-- BEGIN: CONTENT/SHOPS/SHOP-2-7 -->
                <div class="c-bs-grid-small-space">
                    <div class="row">
                        @if(!empty($products))
                        @foreach($products as $product)
                            <div class="col-md-4 col-sm-6 c-margin-b-20">
                                <div class="c-content-product-2 c-bg-white c-border">
                                    <div class="c-content-overlay">
                                        <div class="c-label c-bg-blue c-font-uppercase c-font-white c-font-13 c-font-bold">Hire</div>
                                        <div class="c-overlay-wrapper">
                                            <div class="c-overlay-content">
                                                <a href="{{url("product/hiredetails/$product->id")}}" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">View Details</a>
                                            </div>
                                        </div>
                                        <div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 300px; background-image: url({{ $product->image }});"></div>
                                    </div>
                                    <div class="c-info">
                                        <p class="c-title c-font-16 c-font-slim" style="height: 30px">{{ $product-> name }}</p>
                                    </div>
                                    <div class="btn-group btn-group-justified" role="group">
                                        {{--<div class="btn-group c-border-top" role="group">--}}
                                        {{--<a href="{{url('product/hire')}}" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Hire</a>--}}
                                        {{--</div>--}}
                                        <div class="btn-group c-border-left c-border-top" role="group">
                                            <form action="/cart/hire" method="POST">
                                                {!! csrf_field() !!}
                                                <input type="hidden" name="id" value="{{ $product->id }}">
                                                <input type="hidden" name="name" value="{{ $product->name }}">
                                                <input type="hidden" name="price" value="{{ $product->hire_price }}">
                                                {{--<input type="submit" class="btn btn-success btn-lg" value="Add to Cart">--}}
                                                <input type="submit" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product" value="Hire Item">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                            @else
                                <div class="c-content-product-2 c-bg-white">
                                    <div class="col-md-12">
                                        <div class="c-info-list">
                                            <h3 class="c-title c-font-bold c-font-22 c-font-dark">
                                                <a class="c-theme-link" href="#"> Seems we don't have items under applied filters</a>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                        @endif
                    </div>
                </div>
                <!-- END: CONTENT/SHOPS/SHOP-2-7 -->
                <div class="c-margin-t-20"></div>
                <ul class="c-content-pagination c-square c-theme pull-right">
                    {{ $products->links() }}
                </ul>

            </div>

            @include('partials.offers')


            @include('partials.clients')
        </div>
    </div>
    <!-- END: PAGE CONTAINER -->


    <!-- Page-Level Scripts -->
    <script type='text/javascript' charset="utf-8">
        $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var entity = "Product";

            $("#category_sort").click(function () {

                var id = $("#category_sort_id").val();
                $.ajax({
                    type: "POST",
                    url: "hire",
                    data: {category: id}
                });
            });


        });
    </script>
@endsection
