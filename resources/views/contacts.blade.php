<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 4/29/2017
 * Time: 4:33 AM
 */?>
@extends('layouts.master')
@section('content')
<!-- BEGIN: PAGE CONTAINER -->
<div class="c-layout-page">
    <!-- BEGIN: PAGE CONTENT -->
    <!-- BEGIN: CONTENT/CONTACT/CONTACT-1 -->
    <div class="c-content-box c-size-md c-bg-img-top c-no-padding c-pos-relative">
        <div class="container">
            <div class="c-content-contact-1 c-opt-1">
                <div class="row" data-auto-height=".c-height">
                    <div class="col-sm-8 c-desktop"></div>
                    <div class="col-sm-4">
                        <div class="c-body">
                            <div class="c-section">
                                <h3>Medir Systems.</h3>
                            </div>
                            <div class="c-section">
                                <div class="c-content-label c-font-uppercase c-font-bold c-theme-bg">Address</div>
                                <p>Main Office: Suraj Plaza(Opposite Jamhuri School),
                                    <br/>5th Floor , Suite 501
                                    <br/>Limuru Road Parklands, (Opposite Jamhuri High School)
                                 </p>
                            </div>
                            <div class="c-section">
                                <div class="c-content-label c-font-uppercase c-font-bold c-theme-bg">Contacts</div>
                                <p>
                                    <strong>Main</strong> 0725 693 748
                                    <br/>
                                    <strong>Other</strong> 0722 605 682</p>
                            </div>
                            <div class="c-section">
                                <div class="c-content-label c-font-uppercase c-font-bold c-theme-bg">Social</div>
                                <br/>
                                <ul class="c-content-iconlist-1 c-theme">
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-youtube-play"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-linkedin"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="gmapbg" class="c-content-contact-1-gmap" style="height: 615px;"></div>
    </div>
    <!-- END: CONTENT/CONTACT/CONTACT-1 -->
    <!-- BEGIN: CONTENT/CONTACT/FEEDBACK-2 -->
    <div class="c-content-box c-size-md c-no-padding c-bg-img-center" style="background-image:url(assets/base/img/content/backgrounds/suraj_bg.jpg);">
        <div class="container">
            <div class="c-content-feedback-1 c-option-2">
                <div class="c-content-title-1">
                    <h3 class="c-center c-font-white c-font-bold c-font-uppercase "> MEDIR
                        <span class="c-font-white">SYSTEMS</span>
                    </h3>
                    <div class="c-line-center c-left"></div>
                    <div class="c-line-center c-right"></div>
                    <p class="c-center c-font-white "> Affordable Services &nbsp;
                        <span class="c-font-white">&nbsp;&nbsp;Always The Best</span>
                    </p>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="c-container c-bg-white c-bg-img-bottom-right" style="background-image:url(assets/base/img/content/misc/feedback_box_2.png)">
                            <div class="c-content-title-1">
                                <h3 class="c-font-uppercase c-font-bold"> Our Products ..</h3>
                                <div class="c-line-left"></div>
                                <div class="row">
                                    <div data-slider="owl">
                                        <div class="owl-carousel owl-theme c-theme owl-small-space c-owl-nav-center" data-rtl="false" data-items="2" data-slide-speed="8000">
                                            @foreach($products as $product)
                                                <div class="item">
                                                    <div class="c-content-product-2 c-bg-white c-border">
                                                        <div class="c-content-overlay">
                                                            <div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold">Sale</div>
                                                            <div class="c-overlay-wrapper">
                                                                <div class="c-overlay-content">
                                                                    <a href="{{url("product/details/$product->id")}}" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">View Details</a>
                                                                </div>
                                                            </div>
                                                            <div class="c-bg-img-center-contain c-overlay-object" data-height="height" style="height: 270px; background-image: url({{ $product->thumbnail }});"></div>
                                                        </div>
                                                        <div class="c-info">
                                                            <p class="c-title c-font-18 c-font-slim">{{ $product-> name }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 c-bg-white">
                        <div class="c-contact">
                            <div class="c-content-title-1">
                                <h3 class="c-font-uppercase c-font-bold">Contact Us</h3>
                                <div class="c-line-left"></div>
                                <p class="c-font-lowercase">Our helpline is always open to receive any inquiry or feedback. Please feel free to drop us an email from the form below and we will get back to you as soon as we can.</p>
                            </div>
                            <form action="{{ url('contacts') }}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <input type="text" placeholder="Your Name" name="name" class="form-control c-square c-theme input-lg"> </div>
                                <div class="form-group">
                                    <input type="email" placeholder="Your Email" name="email" class="form-control c-square c-theme input-lg"> </div>
                                <div class="form-group">
                                    <input type="text" placeholder="Contact Phone" name="phone" class="form-control c-square c-theme input-lg"> </div>
                                <div class="form-group">
                                    <textarea rows="8" name="message" placeholder="Write comment here ..." class="form-control c-theme c-square input-lg"></textarea>
                                </div>
                                <button type="submit" class="btn c-theme-btn c-btn-uppercase btn-lg c-btn-bold c-btn-square">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: CONTENT/CONTACT/FEEDBACK-2 -->
    <!-- END: PAGE CONTENT -->
</div>
<!-- END: PAGE CONTAINER -->
@endsection