<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 4/30/2017
 * Time: 11:32 PM
 */?>
@extends('layouts.master')
@section('content')
<!-- BEGIN: PAGE CONTAINER -->
<div class="c-layout-page">
    <!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
    <div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
        <div class="container">
            <div class="c-page-title c-pull-left">
                <h3 class="c-font-uppercase c-font-sbold">Cart</h3>
            </div>
            <ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
                <li>
                    <a href="{{url('product')}}">Products</a>
                </li>
                <li>/</li>
                <li class="c-state_active">Shopping Cart</li>
            </ul>
        </div>
    </div>
    <!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
    <!-- BEGIN: PAGE CONTENT -->
    <!-- BEGIN: CONTENT/SHOPS/SHOP-CART-1 -->
    @if (sizeof(Cart::instance('default')->content()) > 0)
    <div class="c-content-box c-size-sm">
        <div class="container">
            <div class="c-shop-cart-page-1">
                <div class="row c-cart-table-title">
                    <div class="col-md-2 c-cart-image">
                        <h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Image</h3>
                    </div>
                    <div class="col-md-4 c-cart-desc">
                        <h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Product</h3>
                    </div>
                    <div class="col-md-1 c-cart-qty">
                        <h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Qty</h3>
                    </div>
                    <div class="col-md-2 c-cart-price">
                        <h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Unit Price</h3>
                    </div>
                    <div class="col-md-1 c-cart-total">
                        <h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Total</h3>
                    </div>
                    <div class="col-md-1 c-cart-remove"></div>
                </div>
                <!-- BEGIN: SHOPPING CART ITEM ROW -->
                @foreach ($contents as $item)
                <div class="row c-cart-table-row">
                    <h2 class="c-font-uppercase c-font-bold c-theme-bg c-font-white c-cart-item-title c-cart-item-first">Item 1</h2>
                    <div class="col-md-2 col-sm-3 col-xs-5 c-cart-image">
                        <a href="{{url('product/details',$item->id)}}">
                            <img src="{{asset(\App\Products::find($item->id)->thumbnail)}}" />
                        </a>
                    </div>
                    <div class="col-md-4 col-sm-9 col-xs-7 c-cart-desc">
                        <h3>
                            <a href="{{url('product/details',$item->id)}}" class="c-font-bold c-theme-link c-font-22 c-font-dark">{{ $item->name }}</a>
                        </h3>
                    </div>
                    <div class="col-md-1 col-sm-3 col-xs-6 c-cart-qty">
                        <p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">QTY</p>
                        <div class="c-input-group c-spinner">
                            <div class="c-input-group-btn-vertical">
                                <select class="quantity" data-id="{{ $item->rowId }}">
                                    <option {{ $item->qty == 1 ? 'selected' : '' }}>1</option>
                                    <option {{ $item->qty == 2 ? 'selected' : '' }}>2</option>
                                    <option {{ $item->qty == 3 ? 'selected' : '' }}>3</option>
                                    <option {{ $item->qty == 4 ? 'selected' : '' }}>4</option>
                                    <option {{ $item->qty == 5 ? 'selected' : '' }}>5</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-3 col-xs-6 c-cart-price">
                        <p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Unit Price</p>
                        <p class="c-cart-price c-font-bold">{{$item -> price}}</p>
                    </div>
                    <div class="col-md-1 col-sm-3 col-xs-6 c-cart-total">
                        <p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Total</p>
                        <p class="c-cart-price c-font-bold">Ksh {{ $item->subtotal }}</p>
                    </div>
                    <div class="col-md-1 col-sm-12 c-cart-remove">
                        <form action="{{ url('cart', [$item->rowId]) }}" method="POST" class="side-by-side">
                            {!! csrf_field() !!}
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="submit" class="c-theme-link c-cart-remove-desktop" value="x">
                            <input type="submit" class="c-cart-remove-mobile btn c-btn c-btn-md c-btn-square c-btn-red c-btn-border-1x c-font-uppercase" value="Remove item from Cart">
                            {{--<input type="submit" class="btn btn-danger btn-sm" value="Remove">--}}
                        </form>

                    </div>
                </div>
            @endforeach
                <!-- END: SHOPPING CART ITEM ROW -->
                <!-- BEGIN: SUBTOTAL ITEM ROW -->
                <div class="row" style="padding: 0 20%">
                    <div class="c-cart-subtotal-row c-margin-t-20">
                        <div class="col-md-2 col-md-offset-9 col-sm-6 col-xs-6 c-cart-subtotal-border">
                            <h3 class="c-font-uppercase c-font-bold c-right c-font-16 c-font-grey-2">Subtotal</h3>
                        </div>
                        <div class="col-md-1 col-sm-6 col-xs-6 c-cart-subtotal-border">
                            <h3 class="c-font-bold c-font-16">Ksh {{ Cart::instance('default')->subtotal() }}</h3>
                        </div>
                    </div>
                </div>
                <!-- END: SUBTOTAL ITEM ROW -->
                <!-- BEGIN: SUBTOTAL ITEM ROW -->
                <div class="row" style="padding: 0 20%">
                    <div class="c-cart-subtotal-row">
                        <div class="col-md-2 col-md-offset-9 col-sm-6 col-xs-6 c-cart-subtotal-border">
                            <h3 class="c-font-uppercase c-font-bold c-right c-font-16 c-font-grey-2">Grand Total</h3>
                        </div>
                        <div class="col-md-1 col-sm-6 col-xs-6 c-cart-subtotal-border">
                            <h3 class="c-font-bold c-font-16">Ksh {{ Cart::instance('default')->subtotal() }}</h3>
                        </div>
                    </div>
                </div>
                <!-- END: SUBTOTAL ITEM ROW -->
                <div class="c-cart-buttons"  style="padding: 0 17%">
                    <a href="{{url('product')}}" class="btn c-btn btn-lg c-btn-red c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-l">Continue Shopping</a>
                    <a href="{{url('cart/checkout')}}" class="btn c-btn btn-lg c-theme-btn c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-r">Checkout</a>
                </div>
            </div>
        </div>
    </div>
    @else
        <div class="c-content-box c-size-lg c-overflow-hide c-bg-white">
            <div class="container">
                <div class="c-shop-order-complete-1 c-content-bar-1 c-align-left c-bordered c-theme-border c-shadow">
                    <div class="c-content-title-1">
                        <h3 class="c-center c-font-uppercase c-font-bold">You have no items in your shopping cart</h3>
                        <div class="c-line-center c-theme-bg"></div>
                    </div>
                    <div class="c-theme-bg">
                        <a href="{{url('product')}}" class="c-message c-center c-font-white c-font-20 c-font-sbold">
                            <i class="fa fa-check"></i> Continue Shopping. </a>
                    </div>
                 </div>
            </div>
         </div>
        {{--<h3>You have no items in your shopping cart</h3>--}}
        {{--<a href="{{ url('/product') }}" class="btn btn-primary btn-sm">Continue Shopping</a>--}}

    @endif
    <!-- END: CONTENT/SHOPS/SHOP-CART-1 -->
    <!-- BEGIN: CONTENT/SHOPS/SHOP-2-2 -->
    <div class="c-content-box c-size-sm c-overflow-hide c-bs-grid-small-space">
        <div class="container">
            <div class="c-content-title-4">
                <h3 class="c-font-uppercase c-center c-font-bold c-line-strike">
                    <span class="c-bg-white">Most Popular</span>
                </h3>
            </div>
            <div class="row">
                <div data-slider="owl">
                    <div class="owl-carousel owl-theme c-theme owl-small-space c-owl-nav-center" data-rtl="false" data-items="4" data-slide-speed="8000">
                        @foreach($items as $item)
                            <div class="item">
                                <div class="c-content-product-2 c-bg-white c-border">
                                    <div class="c-content-overlay">
                                        <div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold">Sale</div>
                                        <div class="c-overlay-wrapper">
                                            <div class="c-overlay-content">
                                                <a href="{{url('product/details/'.$item->id)}}" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
                                            </div>
                                        </div>
                                        <div class="c-bg-img-center-contain c-overlay-object" data-height="height" style="height: 270px; background-image: url({{asset($item->image)}});"></div>
                                    </div>
                                    <div class="c-info">
                                        <p class="c-title c-font-18 c-font-slim">{{ $item -> name }}</p>
                                        <p class="c-price c-font-16 c-font-slim">{{ $item -> price }}
                                        </p>
                                    </div>
                                    <div class="btn-group btn-group-justified" role="group">
                                        <div class="btn-group c-border-top" role="group">
                                            <a href="#" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Hire</a>
                                        </div>
                                        <div class="btn-group c-border-left c-border-top" role="group">
                                            <form action="/cart" method="POST">
                                                {!! csrf_field() !!}
                                                <input type="hidden" name="id" value="{{ $item->id }}">
                                                <input type="hidden" name="name" value="{{ $item->name }}">
                                                @if($item->on_offer == 1)
                                                    <input type="hidden" name="price" value="{{ $item->offer_price }}">
                                                @else
                                                    <input type="hidden" name="price" value="{{ $item->price }}">
                                                @endif
                                                <input type="submit" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product" value="Add to Cart">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: CONTENT/SHOPS/SHOP-2-2 -->

    <!-- BEGIN: CONTENT/STEPS/STEPS-3 -->
    <div class="c-content-box c-size-md c-theme-bg">
        <div class="container">
            <div class="c-content-step-3 c-font-white">
                <div class="row">
                    <div class="col-md-4 c-steps-3-block">
                        <i class="fa fa-truck"></i>
                        <div class="c-steps-3-title">c
                            <h2 class="c-font-white c-font-uppercase c-font-30 c-font-thin">Free shipping</h2>
                            <em>Express delivery withing 3 days</em>
                        </div>
                        <span>&nbsp;</span>
                    </div>
                    <div class="col-md-4 c-steps-3-block">
                        <i class="fa fa-gift"></i>
                        <div class="c-steps-3-title">
                            <h2 class="c-font-white c-font-uppercase c-font-30 c-font-thin">Daily Gifts</h2>
                            <em>3 Gifts daily for lucky customers</em>
                        </div>
                        <span>&nbsp;</span>
                    </div>
                    <div class="col-md-4 c-steps-3-block">
                        <i class="fa fa-phone"></i>
                        <div class="c-steps-3-title">
                            <h2 class="c-font-white c-font-uppercase c-font-30 c-font-thin">477 505 8877</h2>
                            <em>24/7 customer care available</em>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: CONTENT/STEPS/STEPS-3 -->
    <!-- END: PAGE CONTENT -->
</div>
<!-- END: PAGE CONTAINER -->

<script type='text/javascript' charset="utf-8">
    $(document).ready(function(){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.quantity').on('change', function() {
            var id = $(this).attr('data-id')
            $.ajax({
                type: "PATCH",
                url: '{{ url("/cart") }}' + '/' + id,
                data: {
                    'quantity': this.value,
                },
                success: function(data) {
                    window.location.href = '{{ url('/cart') }}';
                }
            });

        });

    });
</script>
@endsection
