<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 4/29/2017
 * Time: 4:33 AM
 */?>
@extends('layouts.master')
    @section('content')
        <!-- BEGIN: PAGE CONTAINER -->
        <div class="c-layout-page">
            <!-- BEGIN: PAGE CONTENT -->
            <!-- BEGIN: CONTENT/FEATURES/FEATURES-13-4 -->
            <!-- BEGIN: FEATURES 13.4 -->
            <div class="c-content-box c-size-md c-no-padding c-bg-img-center" style="background-image: url(assets/base/img/content/backgrounds/about1.jpg)">
                <div class="c-content-feature-13">
                    <div class="row c-reset">
                        <div class="col-md-8 col-md-offset-4 c-bg-white">
                            <div class="c-feature-13-container">
                                <div class="c-content-title-1">
                                    <h3 class="c-font-uppercase c-font-bold">Our
                                        <span class="c-theme-font">Goal</span>
                                    </h3>
                                    <div class="c-line-left c-theme-bg"></div>
                                </div>
                                <div class="row">
                                    <div class="c-feature-13-tile">
                                        <i class="icon-users c-theme-font c-font-24"></i>
                                        <div class="c-feature-13-content">
                                            <h4 class="c-font-uppercase c-theme-font c-font-bold">Our Goal</h4>
                                            <p class="c-font-dark">To provide strategic and technical value to our esteemed customers through provision of affordable
                                                specialized surveying and Engineering Equipment for both sale and hire.</p>
                                        </div>
                                    </div>
                                    <div class="c-feature-13-tile">
                                        <i class="icon-heart c-theme-font c-font-24"></i>
                                        <div class="c-feature-13-content">
                                            <h4 class="c-font-uppercase c-theme-font c-font-bold">Our Vision</h4>
                                            <p class="c-font-dark">To offer specialized Equipment in the field of surveying and engineering by providing accurate and precise equipment to
                                                various players/stakeholders in infrastructure/utility development and property management.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END: FEATURES 13-4 -->
            <!-- END: CONTENT/FEATURES/FEATURES-13-4 -->
            <!-- BEGIN: CONTENT/FEATURES/FEATURES-13-3 -->
            <!-- BEGIN: FEATURES 13.3 -->
            <div class="c-content-box c-size-md c-no-padding c-bg-img-center" style="background-image: url(assets/base/img/content/backgrounds/about2.jpg)">
                <div class="c-content-feature-13">
                    <div class="row c-reset">
                        <div class="col-md-8 c-bg-dark">
                            <div class="c-feature-13-container">
                                <div class="c-content-title-1">
                                    <h3 class="c-font-uppercase c-font-white c-font-bold">Our
                                        <span class="c-theme-font">Mission</span>
                                    </h3>
                                    <div class="c-line-left c-theme-bg"></div>
                                </div>
                                <div class="row">

                                    <div class="c-feature-13-tile">
                                        <i class="icon-picture c-theme-font c-font-24"></i>
                                        <div class="c-feature-13-content">
                                            <h4 class="c-font-uppercase c-theme-font c-font-bold">Mission</h4>
                                            <p class="c-font-grey">We endeavor to be the best measurement systems provider and users in East Africa and Africa at large by improving the standards
                                                and the quality of the projects undertaken by our stakeholders through better and affordable instruments customized for each project.</p>
                                        </div>
                                    </div>
                                    <div class="c-feature-13-tile">
                                        <i class="icon-size-fullscreen c-theme-font c-font-24"></i>
                                        <div class="c-feature-13-content">
                                            <h4 class="c-font-uppercase c-theme-font c-font-bold">Our Company</h4>
                                            <p class="c-font-grey">Medir Systems Limited is a middle sized Engineering, Agricultural and GPS Navigation Equipment Company with its head office in
                                                Nairobi  at Suraj Plaza opp Jamhuri Secondary School. The Company offers a full range of equipments to a diversified client
                                                base ranging from individuals to well-known multinational corporations resident in Kenya.</p>
                                        </div>
                                    </div>
                                    <div class="c-feature-13-tile">
                                        <i class="icon-puzzle c-theme-font c-font-24"></i>
                                        <div class="c-feature-13-content">
                                            <h4 class="c-font-uppercase c-theme-font c-font-bold">Our Clients</h4>
                                            <p class="c-font-grey">Our clients depend on the quality,
                                                standards and affordability of our products. Our products are available for both sale and hire. The company’s main Partners
                                                are South Systems, Leica, Hi- Target, Ruide Instruments, TopCon, Sanding, and FOIF.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END: FEATURES 13-3 -->
            <!-- END: PAGE CONTENT -->
            @include('partials.clients')
        </div>
        <!-- END: PAGE CONTAINER -->
        @endsection
