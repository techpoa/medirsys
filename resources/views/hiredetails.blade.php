<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 5/8/2017
 * Time: 2:39 PM
 */
?>

@extends('layouts.master')
@section('content')
    <!-- BEGIN: PAGE CONTAINER -->
    <div class="c-layout-page">
        <!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
        <div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
            <div class="container">
                <div class="c-page-title c-pull-left">
                    <h3 class="c-font-uppercase c-font-sbold">Product Details</h3>
                </div>
                <ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
                    <li><a href="{{url('products')}}">Product</a></li>
                    <li>/</li>
                    <li class="c-state_active">{{$product->name}}</li>
                </ul>
            </div>
        </div>
        <!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
        <!-- BEGIN: PAGE CONTENT -->
        <!-- BEGIN: CONTENT/SHOPS/SHOP-PRODUCT-DETAILS-2 -->
        <div class="c-content-box c-size-lg c-overflow-hide c-bg-white">
            <div class="container">
                <div class="c-shop-product-details-2">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="c-product-gallery">
                                <div class="c-product-gallery-content">
                                    {{--<div class="c-zoom">--}}
                                    <img src="{{asset($product->image)}}">
                                    {{--</div>--}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="c-product-meta">
                                <div class="c-content-title-1">
                                    <h3 class="c-font-uppercase c-font-bold">{{ $product->name }}</h3>
                                    <div class="c-line-left"></div>
                                </div>
                                <div class="c-product-badge">
                                    <div class="c-product-new">Hire</div>
                                </div>
                                <div class="c-product-review">
                                    <div class="c-product-rating">
                                        <i class="fa fa-star c-font-red"></i>
                                        <i class="fa fa-star c-font-red"></i>
                                        <i class="fa fa-star c-font-red"></i>
                                        <i class="fa fa-star c-font-red"></i>
                                        <i class="fa fa-star-half-o c-font-red"></i>
                                    </div>
                                    <div class="c-product-write-review">
                                        <a class="c-font-black" href="#"><strong>Available :: {{ $product->quantity }}</strong> </a>
                                    </div>
                                </div>
                                <div class="c-product-price">{{ $product->hire_price }}</div>
                                <div class="c-product-short-desc">
                                    {!! $product -> description !!}
                                </div>
                                <div class="c-product-add-cart c-margin-t-20">
                                    <div class="row">
                                        <div class="col-sm-4 col-xs-12">
                                            <div class="c-input-group c-spinner">
                                                <p class="c-product-meta-label c-product-margin-2 c-font-uppercase c-font-bold">QTY:</p>
                                                <input type="text" class="form-control c-item-1" value="1">
                                                <div class="c-input-group-btn-vertical">
                                                    <button class="btn btn-default" type="button" data_input="c-item-1">
                                                        <i class="fa fa-caret-up"></i>
                                                    </button>
                                                    <button class="btn btn-default" type="button" data_input="c-item-1">
                                                        <i class="fa fa-caret-down"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row c-margin-t-20">
                                            <div class="col-md-6">
                                                <form action="/cart/hire" method="POST">
                                                    {!! csrf_field() !!}
                                                    <input type="hidden" name="id" value="{{ $product->id }}">
                                                    <input type="hidden" name="name" value="{{ $product->name }}">
                                                    <input type="hidden" name="price" value="{{ $product->hire_price }}">
                                                    <button type="submit" class="btn c-btn btn-lg c-font-bold c-font-white c-theme-btn c-btn-square c-font-uppercase">Hire Item</button>

                                                </form>
                                            </div>
                                            <div class="col-md-6">
                                                @if(!empty($product->brochure))
                                                    <form action="{{url('product/brochure')}}" method="POST">
                                                        {!! csrf_field() !!}
                                                        <input type="hidden" name="id" value="{{ $product->id }}">
                                                        <button type="submit" class="btn c-btn btn-lg c-font-bold c-font-white c-theme-btn c-btn-square c-font-uppercase pull-right">
                                                            <i class="fa fa-download"></i> Download Brochure</button>
                                                    </form>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END: CONTENT/SHOPS/SHOP-PRODUCT-DETAILS-2 -->
        <!-- BEGIN: CONTENT/SHOPS/SHOP-2-2 -->
        <div class="c-content-box c-size-md c-overflow-hide c-bs-grid-small-space">
            <div class="container">
                <div class="c-content-title-4">
                    <h3 class="c-font-uppercase c-center c-font-bold c-line-strike">
                        <span class="c-bg-white">Similar For Hire Items</span>
                    </h3>
                </div>
                <div class="row">
                    <div data-slider="owl">
                        <div class="owl-carousel owl-theme c-theme owl-small-space c-owl-nav-center" data-rtl="false" data-items="4" data-slide-speed="8000">
                            @foreach($items as $item)
                                <div class="item">
                                    <div class="c-content-product-2 c-bg-white c-border">
                                        <div class="c-content-overlay">
                                            <div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold">Sale</div>
                                            <div class="c-overlay-wrapper">
                                                <div class="c-overlay-content">
                                                    <a href="{{url('product/details/'.$item->id)}}" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
                                                </div>
                                            </div>
                                            <div class="c-bg-img-center-contain c-overlay-object" data-height="height" style="height: 270px; background-image: url({{asset($item->image)}});"></div>
                                        </div>
                                        <div class="c-info">
                                            <p class="c-title c-font-18 c-font-slim">{{ $item -> name }}</p>
                                        </div>
                                        <div class="btn-group btn-group-justified" role="group">
                                            <div class="btn-group c-border-left c-border-top" role="group">
                                                <form action="/cart/hire" method="POST">
                                                    {!! csrf_field() !!}
                                                    <input type="hidden" name="id" value="{{ $item->id }}">
                                                    <input type="hidden" name="name" value="{{ $item->name }}">
                                                    <input type="hidden" name="price" value="{{ $item->hire_price }}">
                                                    <input type="submit" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product" value="Add to Cart">
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END: CONTENT/SHOPS/SHOP-2-2 -->
    </div>
    <!-- END: CONTENT/SHOPS/SHOP-PRODUCT-TAB-1 -->
@endsection
