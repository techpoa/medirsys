<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 4/30/2017
 * Time: 2:13 AM
 */?>

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="header"></li>
            <li class="header"></li>
            <li class="header"></li>
            <li class="treeview {!! Request::is('admin','hire/*') ? 'active' : '' !!}">
                <a href="{{url('admin')}}">
                    <i class="fa fa-dashboard">

                    </i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="treeview {!! Request::is('users') ? 'active' : '' !!}">
                <a href="{{url('users')}}">
                    <i class="fa fa-users"></i>
                    <span>System Users</span>
                </a>
            </li>
            <li class="treeview {!! Request::is('categories','categories/*','products','products/*') ? 'active' : '' !!}">
                <a href="#">
                    <i class="fa fa-list"></i>
                    <span>Products</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{url('categories')}}"><i class="fa fa-circle-o"></i>Product Categories</a></li>
                    <li><a href="{{url('products')}}"><i class="fa fa-circle-o"></i> Products</a></li>
                </ul>
            </li>
            <li class="{!! Request::is('orders') ? 'active' : '' !!}" >
                <a href="{{url('orders')}}">
                    <i class="fa fa-shopping-cart"></i> <span>Orders</span>
                </a>
            </li>
            <li class="{!! Request::is('subscriptions') ? 'active' : '' !!}" >
                <a href="{{url('subscriptions')}}">
                    <i class="fa fa-send"></i> <span>Subscriptions</span>
                </a>
            </li>
            <li class="{!! Request::is('notifications') ? 'active' : '' !!}" >
                <a href="{{url('notifications')}}">
                    <i class="fa fa-bell"></i> <span>Notifications</span>
                </a>
            </li>
            <li class="treeview {!! Request::is('configurations','/admin/profile','/admin/password') ? 'active' : '' !!}">
                <a href="#">
                    <i class="fa fa-wrench"></i> <span>Configurations</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{!! Request::is('configurations') ? 'active' : '' !!}" ><a href="{{url('configurations')}}"><i class="fa fa-file-excel-o"></i> Configurations</a></li>
                    <li class="{!! Request::is('admin','admin/*') ? 'active' : '' !!}" ><a href="{{url('/admin/password')}}"><i class="fa fa-key"></i> Change Password</a></li>

                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
