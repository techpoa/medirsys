<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 4/30/2017
 * Time: 2:57 AM
 */?>
@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="col-md-4">
                <h1>
                    Data Tables
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li><a href="#">Tables</a></li>
                    <li class="active">Data tables</li>
                </ol>
                </div>
            <div class="col-md-8">
                </div>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">

                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="activeRolesTable" class="table table-striped table-bordered table-hover" >
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Slug</th>
                                    <th>Description</th>
                                    <th>Last Edited</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Name</th>
                                    <th>Slug</th>
                                    <th>Description</th>
                                    <th>Last Edited</th>
                                    <th>Actions</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

    {{--MODALS --}}

    <div class="modal inmodal fade" id="createModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span></button>
                    <h4 class="modal-title"> Create Role </h4>
                </div>
                <div class="modal-body">
                    <form id="frmCreate" role="form" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="col-md-12">
                            <label for="name">Role</label>
                            <input type="text" name="name" id="name" class="form-control">
                        </div>
                        <div class="col-md-12">
                            <label for="slug">Slug</label>
                            <input type="text" name="slug" id="slug" class="form-control">
                        </div>
                        <div class="col-md-12">
                            <label for="description"> Description</label>
                            <textarea name="description" id="description" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <input type="hidden" class="form-control required">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnCloseCreate" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="button" id="btnCreate" class="btn btn-primary">Save Details</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal fade" id="editModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span></button>
                    <h4 class="modal-title"> Edit Role Details </h4>
                </div>
                <div class="modal-body">
                    <form id="frmEdit" role="form" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="col-md-12">
                            <label for="editName"> Name</label>
                            <input type="text" name="editName" id="editName" class="form-control">
                        </div>
                        <div class="col-md-12">
                            <label for="editSlug">Slug</label>
                            <input type="text" name="editSlug" id="editSlug" class="form-control" readonly>
                        </div>
                        <div class="col-md-12">
                            <label for="editDescription"> Description</label>
                            <textarea name="editDescription" id="editDescription" class="form-control"></textarea>
                        </div>

                        <input type="hidden" id="roleID">

                        <div class="form-group">
                            <input type="hidden" class="form-control required">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnCloseEdit" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="button" id="btnSaveEdit" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    @include('admin.partials.modals')

    <!-- Page-Level Scripts -->
    <script type='text/javascript' charset="utf-8">
        $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var entity = "Roles";
            var createModal = $('#createModal');
            var frmCreate = document.getElementById("frmCreate");
            var editModal = $('#editModal');
            var frmEdit = document.getElementById("frmEdit");
            var deleteModal = $('#deleteModal');

            var activeRolesTable = $('#activeRolesTable').DataTable({
                pageLength: 10,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'SchoolERP System Roles'},
                    {extend: 'pdf', title: 'SchoolERP System Roles'},
                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                        }
                    }
                ],
                "ajax":  "/activeroles",
                "columns": [
                    {data: 'name', name: 'name'},
                    {data: 'slug', name: 'slug'},
                    {data: 'description', name: 'description'},
                    {data: 'updated_at', name: 'updated_at'},
                    {data: 'actions', name: 'actions'}
                ]
            });

            $(document).on("click", ".edit", function () {

                var btnSaveEdit = $("#btnSaveEdit");
                var id = $(this).data('id');
                $.ajax({
                    type: "GET",
                    url: "roles/"+id+"/edit",
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    document.getElementById("roleID").value = id;
                                    document.getElementById("editName").value = data.name;
                                    document.getElementById("editSlug").value = data.slug;
                                    document.getElementById("editDescription").value = data.description;
                                    $('#editModal').modal('show');
                                    break;

                                } else if (data.status === '01') {
                                    activeRolesTable.ajax.reload( null, false );
                                    toastr.success('Success!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $("#btnSaveEdit").click(function () {

                var name = $("#editName").val();
                var slug = $("#editSlug").val();
                var description = $("#editDescription").val();
                var id = $("#roleID").val();
                $.ajax({
                    type: "PATCH",
                    url: "roles/"+id,
                    data: { id: id, name: name, slug: slug, description: description},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    $('#editModal').modal('toggle');
                                    frmEdit.reset();
                                    activeRolesTable.ajax.reload( null, false );
                                    toastr.success('Success', data.message);
                                    break;
                                } else if (data.status === '01') {
                                    activeRolesTable.ajax.reload( null, false );
                                    toastr.error('Error!', data.message);                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on('click', 'a.delete', function () {

                var id = $(this).data('id'); // get the item ID
                var name = $(this).data('name'); // get the item name
                document.getElementById("deleteID").value = id;
                $("#deleteTitle").html("Delete " + entity);
                $("#deleteNotification").html("Are you sure you want to delete " + entity + " <span class='text-danger'>" + name + "</span>");
            });

            $('#btnDelete').click(function () {

                var id = $("#deleteID").val();
                $.ajax({
                    type: "DELETE",
                    url: "roles/"+id,
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    activeRolesTable.ajax.reload( null, false );
                                    toastr.success('Success!', data.message);
                                } else if (data.status === '01') {
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            setInterval( function () {
                activeRolesTable.ajax.reload( null, false ); // roles paging is not reset on reload
            }, 30000 );

        });
    </script>

    @endsection

