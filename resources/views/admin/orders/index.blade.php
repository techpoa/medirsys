<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 4/30/2017
 * Time: 2:18 PM
 */

?>

@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="col-md-6">
                <h1>
                    Orders
                </h1>
                <ol class="breadcrumb">
                    <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Orders</li>
                </ol>
            </div>
            <div class="col-md-6">
                {{--<div class="title-action">--}}
                    {{--<a href="{{url('orders/create')}}"  class="btn btn-sm btn-primary pull-right" > Create Order</a>--}}
                {{--</div>--}}
            </div>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="ordersTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>#</th>
                                    <th> Num </th>
                                    <th> Type </th>
                                    <th> Name </th>
                                    <th> Address </th>
                                    <th> Phone </th>
                                    <th> Start Date </th>
                                    <th> End Date </th>
                                    <th> Status </th>
                                    <th> Price </th>
                                    <th> Last Updated </th>
                                    <th> Action </th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>#</th>
                                    <th> Num </th>
                                    <th> Type </th>
                                    <th> Name </th>
                                    <th> Address </th>
                                    <th> Phone </th>
                                    <th> Start Date </th>
                                    <th> End Date </th>
                                    <th> Status </th>
                                    <th> Price </th>
                                    <th> Last Updated </th>
                                    <th> Action </th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

    {{--MODALS --}}

    @include('admin.partials.modals')

    <!-- Page-Level Scripts -->
    <script type='text/javascript' charset="utf-8">
        $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var entity = "Order";
            var activateModal = $('#activateModal');
            var deactivateModal = $('#deactivateModal');
            var deleteModal = $('#deleteModal');


            function ordersTabl ( data ) {
                // `d` is the original data object for the row

                var  response =

                    '<table class="table table-bordered table-primary">'+
                    '<thead>'+
                    '<tr>'+
                    '<th>Product</th>'+
                    '<th>Quantity </th>'+
                    '<th>Price</th>'+
                    '</tr>'+
                    '</thead>'+
                    '<tbody>';

                $(data.data).each(function (index, value) {

                    response += '<tr>';
                    response += '<td>' + value.product + '</td>';
                    response += '<td>' + value.quantity + '</td>';
                    response += '<td>' + value.price + '</td>';
                    response += '</tr>';
                });

                response += '</tbody>'
                    + '</table>';
                return response;

            }

            var ordersTable = $('#ordersTable').DataTable({
                pageLength: 10,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'Medir System Orders'},
                    {extend: 'pdf', title: 'Medir System Orders'},
                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ],
                "ajax":  "/activeorders",
                "columnDefs": [
                    {
                        "visible": false,
                        "targets": [1]
                    },
                    {
                        "targets": 9,
                        "data": "status",
                        "render": function ( data, type, full, meta ) {
                            switch(data) {
                                case '1' : return '<span class="badge bg-default"> New </span>'; break;
                                case '2' : return '<span class="badge bg-green"> Confirmed </span>'; break;
                                case '3' : return '<span class="badge bg-red"> Declined </span>'; break;
                                default  : return '-';
                            }
                        }
                    }
                ],
                "columns": [
                    {
                        "className":      'details-control',
                        "orderable":      false,
                        "data":           null,
                        "defaultContent": ''
                    },
                    {data: 'id', name: 'id'},
                    {data: 'order_number', name: 'order_number'},
                    {data: 'order_type', name: 'order_type'},
                    {data: 'name', name: 'name'},
                    {data: 'address', name: 'address'},
                    {data: 'phone', name: 'phone'},
                    {data: 'start_date', name: 'start_date'},
                    {data: 'end_date', name: 'end_date'},
                    {data: 'status', name: 'status'},
                    {data: 'total', name: 'total'},
                    {data: 'updated_at', name: 'updated_at'},
                    {data: 'actions', name: 'actions'}
                ]
            });

            // Add event listener for opening and closing details
            $('#ordersTable tbody').on('click', 'td.details-control', function () {
                var tr = $(this).closest('tr');
                var row = ordersTable.row( tr );

                if ( row.child.isShown() ) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                }
                else {
                    // Open this row
                    row.child('Please wait...');
                    row.child.show();
                        $.get("/orderdetailsdata?id="+row.data().id,
                            row.data().id, function (data) {
                                if (typeof ordersTabl !== "undefined") {
                                    row.child( ordersTabl(data) ).show();
                                }
                                tr.addClass('shown');
                            }).fail(function () {
                        row.child('Failed. Please try again.');
                        row.child.show();
                    });
                }
            });


            // Add event listener for opening and closing details
            $('#approvedTable tbody').on('click', 'td.details-control', function () {
                var tr = $(this).closest('tr');
                var row = clientPaymentsTable.row( tr );

                if ( row.child.isShown() ) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                }
                else {
                    // Open this row
                    row.child('Please wait...');
                    row.child.show();
                    $.get("/paymentdetailsdata?id="+row.data().id,
                        row.data().id, function (data) {
                            if (typeof clientPaymentsTabl !== "undefined") {
                                row.child( clientPaymentsTabl(data) ).show();
                            }
                            tr.addClass('shown');
                        }).fail(function () {
                        row.child('Failed. Please try again.');
                        row.child.show();
                    });
                }
            } );


            $(document).on('click', 'a.activate', function () {

                var id = $(this).data('id'); // get the item ID
                var name = $(this).data('name'); // get the item name
                document.getElementById("activateID").value = id;
                $("#activateTitle").html("Accept " + entity);
                $("#activateNotification").html("Are you sure you want to accept " + entity + " <span class='text-danger'>" + name + "</span>");
            });

            $('#btnActivate').click(function () {

                var id = $("#activateID").val();
                $.ajax({
                    type: "POST",
                    url: "orders/accept",
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    activateModal.modal('hide');
                                    activeOrdersTable.ajax.reload( null, false );
                                    toastr.success('Success', data.message);
                                } else if (data.status === '01') {
                                    activeOrdersTable.ajax.reload( null, false );
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on('click', 'a.deactivate', function () {

                var id = $(this).data('id'); // get the item ID
                var name = $(this).data('name'); // get the item name
                document.getElementById("deactivateID").value = id;
                $("#deactivateTitle").html("Decline " + entity);
                $("#deactivateNotification").html("Are you sure you want to decline " + entity + " <span class='text-danger'>" + name + "</span>");
            });

            $('#btnDeactivate').click(function () {

                var id = $("#deactivateID").val();
                $.ajax({
                    type: "POST",
                    url: "orders/decline",
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    deactivateModal.modal('hide');
                                    activeOrdersTable.ajax.reload( null, false );
                                    toastr.success('Success', data.message);
                                } else if (data.status === '01') {
                                    activeOrdersTable.ajax.reload( null, false );
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on('click', 'a.delete', function () {

                var id = $(this).data('id'); // get the item ID
                var name = $(this).data('name'); // get the item name
                document.getElementById("deleteID").value = id;
                $("#deleteTitle").html("Delete " + entity);
                $("#deleteNotification").html("Are you sure you want to delete " + entity + " <span class='text-danger'>" + name + "</span>");
            });


            $('#btnDelete').click(function () {

                var id = $("#deleteID").val();
                $.ajax({
                    type: "DELETE",
                    url: "orders/"+id,
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    activeOrdersTable.ajax.reload( null, false );
                                    toastr.success('Success!', data.message);
                                } else if (data.status === '01') {
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });


        });
    </script>

@endsection