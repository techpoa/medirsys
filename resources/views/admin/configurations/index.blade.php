<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 4/30/2017
 * Time: 2:21 PM
 */

?>

@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="col-md-6">
                <h1>
                    Configurations
                </h1>
                <ol class="breadcrumb">
                    <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Configurations</li>
                </ol>
            </div>
            <div class="col-md-6">

            </div>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                @include('admin.partials.flash-message')
                <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Configuration Details</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" action="{{url('configurations')}}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="name">Company Name</label>
                                    <input type="text" class="form-control" id="name" name="name">
                                </div>
                                <div class="form-group">
                                    <label for="main_address">Main Address </label>
                                    <input type="text" class="form-control" id="main_address" name="main_address">
                                </div>
                                <div class="form-group">
                                    <label for="other_address"> Other Address</label>
                                    <input type="text" class="form-control" id="other_address" name="other_address" >
                                </div>
                                <div class="form-group">
                                    <label for="main_phone">Main Phone</label>
                                    <input type="text" class="form-control" id="main_phone" name="main_phone" placeholder="Enter Price">
                                </div>
                                <div class="form-group">
                                    <label for="other_phone">Other Phone</label>
                                    <input type="text" class="form-control" id="other_phone" name="other_phone" placeholder="Enter Price">
                                </div>
                                <div class="form-group">
                                    <label for="admin_email">Administrator Email </label>
                                    <input type="text" class="form-control" id="admin_email" name="admin_email" placeholder="Enter Price">
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.box -->

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

    {{--MODALS --}}

    @include('admin.partials.modals')

    <!-- Page-Level Scripts -->
    <script type='text/javascript' charset="utf-8">
        $(document).ready(function(){


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

        });
    </script>

@endsection

