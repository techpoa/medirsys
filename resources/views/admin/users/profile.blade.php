<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 5/7/2017
 * Time: 6:41 PM
 */

?>

@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="col-md-6">
                <h1>
                    Profile
                </h1>
                <ol class="breadcrumb">
                    <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">My Profile</li>
                </ol>
            </div>
            <div class="col-md-6">

            </div>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                @include('admin.partials.flash-message')
                <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Configuration Details</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" action="{{url('admin/profile')}}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="name">Names</label>
                                    <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}">
                                </div>
                                <div class="form-group">
                                    <label for="sname">Surname</label>
                                    <input type="text" class="form-control" id="sname" name="sname" value="{{ $user->sname }}">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}">
                                </div>
                                <div class="form-group">
                                    <label for="phone">Phone</label>
                                    <input type="text" class="form-control" id="phone" name="phone" value="{{ $user->phone }}">
                                </div>
                                <div class="form-group">
                                    <label for="file">My Image</label>
                                    <p class="help-block"> Make sure the image is of ideal quality and and square in dimensions.</p>
                                    <input type="file" id="file" name="file">
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.box -->

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

    {{--MODALS --}}

    @include('admin.partials.modals')

    <!-- Page-Level Scripts -->
    <script type='text/javascript' charset="utf-8">
        $(document).ready(function(){


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

        });
    </script>

@endsection

