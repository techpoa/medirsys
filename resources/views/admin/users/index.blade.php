<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 4/30/2017
 * Time: 2:42 AM
 */
?>
@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="col-md-6">
                <h1>
                    System Users
                </h1>
                <ol class="breadcrumb">
                    <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li><a href="#">Administrator</a></li>
                    <li class="active">Users</li>
                </ol>
            </div>
            <div class="col-md-6">
                <div class="title-action" style="margin-top: 50px;">
                    <a href="#" class="btn btn-sm btn-primary pull-right" id="create" data-toggle="modal" data-target="#createModal"> Create User</a>
                </div>
            </div>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="activeUsersTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th> Full Names </th>
                                    <th> Email </th>
                                    <th> Phone </th>
                                    <th> Role </th>
                                    <th> Status </th>
                                    <th> Last Modified</th>
                                    <th> Action </th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th> Full Names </th>
                                    <th> Email </th>
                                    <th> Phone </th>
                                    <th> Role </th>
                                    <th> Status </th>
                                    <th> Last Modified</th>
                                    <th> Action </th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

    {{--MODALS --}}

    <div class="modal inmodal fade" id="createModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span></button>
                    <h4 class="modal-title"> Create Stock Item </h4>
                </div>
                <div class="modal-body">
                    <form id="frmCreate" role="form" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="col-md-6">
                            <label for="name">Full Name</label>
                            <input type="text" name="name" id="name" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <label for="sname">Surname</label>
                            <input type="text" name="sname" id="sname" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <label for="email"> Email</label>
                            <input type="email" name="email" id="email" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <label for="phone"> Phone</label>
                            <input type="text" name="phone" id="phone" class="form-control">
                        </div>
                        <div class="col-md-12">
                            <label for="role">Select</label>
                            <select class="form-control" name="role" id="role">
                                <option> -- Select Role -- </option>
                                @foreach($roles as $role)
                                <option value="{{$role ->  id}}">{{ $role -> name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="hidden" class="form-control required">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnCloseCreate" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="button" id="btnCreate" class="btn btn-primary">Save Details</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal fade" id="editModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span></button>
                    <h4 class="modal-title"> Edit User Details </h4>
                </div>
                <div class="modal-body">
                    <form id="frmEdit" role="form" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="col-md-6">
                            <label for="editName">Full Name</label>
                            <input type="text" name="editName" id="editName" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <label for="editSname">Surname</label>
                            <input type="text" name="editSname" id="editSname" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <label for="editEmail"> Email</label>
                            <input type="email" name="editEmail" id="editEmail" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <label for="editPhone"> Phone</label>
                            <input type="text" name="editPhone" id="editPhone" class="form-control">
                        </div>

                        <div class="col-md-12">
                            <label for="editRole">Select Role</label>
                            <select class="form-control" name="editRole" id="editRole">
                                <option value=""> -- Select Role -- </option>
                                @foreach($roles as $role)
                                    <option value="{{$role ->  id}}">{{ $role -> name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <input type="hidden" id="userID">

                        <div class="form-group">
                            <input type="hidden" class="form-control required">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnCloseEdit" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="button" id="btnSaveEdit" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    @include('admin.partials.modals')

    <!-- Page-Level Scripts -->
    <script type='text/javascript' charset="utf-8">
        $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var entity = "Users";
            var createModal = $('#createModal');
            var frmCreate = document.getElementById("frmCreate");
            var editModal = $('#editModal');
            var frmEdit = document.getElementById("frmEdit");
            var activateModal = $('#activateModal');
            var deactivateModal = $('#deactivateModal');
            var deleteModal = $('#deleteModal');



            var activeUsersTable = $('#activeUsersTable').DataTable({
                pageLength: 10,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'Essays System Users'},
                    {extend: 'pdf', title: 'Essays System Users'},
                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                        }
                    }
                ],
                "ajax":  "/activeusers",
                "columns": [
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'phone', name: 'phone'},
                    {data: 'role_name', name: 'role_name'},
                    {data: 'user_status', name: 'user_status'},
                    {data: 'updated_at', name: 'updated_at'},
                    {data: 'actions', name: 'actions'}
                ]
            });

            $("#btnCreate").click(function () {

                var frm = $('#frmCreate');
                var form = document.getElementById("frmCreate");
                var data = frm.serialize();
                $.ajax({
                    type: "POST",
                    url: "users",
                    data: data,
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    $('#createModal').modal('hide');
                                    frmCreate.reset();
                                    activeUsersTable.ajax.reload( null, false );
                                    toastr.success('Success!', data.message);
                                } else if (data.status === '01') {
                                    activeUsersTable.ajax.reload( null, false );
                                    toastr.error('Error!', data.message);                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on("click", ".edit", function () {

                var btnSaveEdit = $("#btnSaveEdit");
                var id = $(this).data('id');
                $.ajax({
                    type: "GET",
                    url: "users/"+id+"/edit",
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    document.getElementById("userID").value = id;
                                    document.getElementById("editName").value = data.name;
                                    document.getElementById("editSname").value = data.sname;
                                    document.getElementById("editEmail").value = data.email;
                                    document.getElementById("editPhone").value = data.phone;
                                    document.getElementById("editRole").value = data.role;
                                    $('#editModal').modal('show');
                                    break;

                                } else if (data.status === '01') {
                                    activeUsersTable.ajax.reload( null, false );
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $("#btnSaveEdit").click(function () {

                var name = $("#editName").val();
                var sname = $("#editSname").val();
                var email = $("#editEmail").val();
                var phone = $("#editPhone").val();
                var role = $("#editRole").val();
                var id = $("#userID").val();
                $.ajax({
                    type: "PATCH",
                    url: "users/"+id,
                    data: {
                        id: id,
                        name: name,
                        sname: sname,
                        email: email,
                        role_id: role,
                        phone: phone
                    },
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    $('#editModal').modal('hide');
                                    frmEdit.reset();
                                    activeUsersTable.ajax.reload( null, false );
                                    toastr.success('Success!', data.message);
                                } else if (data.status === '01') {
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on('click', 'a.activate', function () {

                var id = $(this).data('id'); // get the item ID
                var name = $(this).data('name'); // get the item name
                document.getElementById("activateID").value = id;
                $("#activateTitle").html("Activate " + entity);
                $("#activateNotification").html("Are you sure you want to activate " + entity + " <span class='text-danger'>" + name + "</span>");
            });

            $('#btnActivate').click(function () {

                var id = $("#activateID").val();
                $.ajax({
                    type: "POST",
                    url: "users/activate",
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    activateModal.modal('hide');
                                    activeUsersTable.ajax.reload( null, false );
                                    toastr.success('Success', data.message);
                                } else if (data.status === '01') {
                                    activeUsersTable.ajax.reload( null, false );
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on('click', 'a.deactivate', function () {

                var id = $(this).data('id'); // get the item ID
                var name = $(this).data('name'); // get the item name
                document.getElementById("deactivateID").value = id;
                $("#deactivateTitle").html("Deactivate " + entity);
                $("#deactivateNotification").html("Are you sure you want to deactivate " + entity + " <span class='text-danger'>" + name + "</span>");
            });

            $('#btnDeactivate').click(function () {

                var id = $("#deactivateID").val();
                $.ajax({
                    type: "POST",
                    url: "users/deactivate",
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    deactivateModal.modal('hide');
                                    activeUsersTable.ajax.reload( null, false );
                                    toastr.success('Success', data.message);
                                } else if (data.status === '01') {
                                    activeUsersTable.ajax.reload( null, false );
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on('click', 'a.delete', function () {

                var id = $(this).data('id'); // get the item ID
                var name = $(this).data('name'); // get the item name
                document.getElementById("deleteID").value = id;
                $("#deleteTitle").html("Delete " + entity);
                $("#deleteNotification").html("Are you sure you want to delete " + entity + " <span class='text-danger'>" + name + "</span>");
            });

            $('#btnDelete').click(function () {

                var id = $("#deleteID").val();
                $.ajax({
                    type: "DELETE",
                    url: "users/"+id,
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    $('#deleteModal').modal('hide');
                                    activeUsersTable.ajax.reload( null, false );
                                    toastr.success('Success!', data.message);
                                } else if (data.status === '01') {
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });


            setInterval( function () {
//                activeUsersTable.ajax.reload( null, false ); // user paging is not reset on reload
//                inactiveUsersTable.ajax.reload( null, false ); // user paging is not reset on reload
            }, 30000 );


        });
    </script>

@endsection