<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 4/30/2017
 * Time: 2:20 PM
 */
?>
@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="col-md-6">
                <h1>
                    Subscriptions
                </h1>
                <ol class="breadcrumb">
                    <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Subscriptions</li>
                </ol>
            </div>
            <div class="col-md-6">
            </div>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="activeSubscriptionsTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th> Email </th>
                                    <th> Last Modified</th>
                                    <th> Action </th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th> Email </th>
                                    <th> Last Modified</th>
                                    <th> Action </th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

    {{--MODALS --}}

    @include('admin.partials.modals')

    <!-- Page-Level Scripts -->
    <script type='text/javascript' charset="utf-8">
        $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var entity = "Subscriptions";
            var activateModal = $('#activateModal');
            var deactivateModal = $('#deactivateModal');
            var deleteModal = $('#deleteModal');



            var activeSubscriptionsTable = $('#activeSubscriptionsTable').DataTable({
                pageLength: 10,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'Essays System Users'},
                    {extend: 'pdf', title: 'Essays System Users'},
                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ],
                "ajax":  "/activesubscriptions",
                "columns": [
                    {data: 'email', name: 'email'},
                    {data: 'updated_at', name: 'updated_at'},
                    {data: 'actions', name: 'actions'}
                ]
            });

            $(document).on('click', 'a.delete', function () {

                var id = $(this).data('id'); // get the item ID
                var name = $(this).data('name'); // get the item name
                document.getElementById("deleteID").value = id;
                $("#deleteTitle").html("Delete " + entity);
                $("#deleteNotification").html("Are you sure you want to delete " + entity + " <span class='text-danger'>" + name + "</span>");
            });

            $('#btnDelete').click(function () {

                var id = $("#deleteID").val();
                $.ajax({
                    type: "DELETE",
                    url: "users/"+id,
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    activeUsersTable.ajax.reload( null, false );
                                    toastr.success('Success!', data.message);
                                } else if (data.status === '01') {
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });


            setInterval( function () {
                activeSubscriptionsTable.ajax.reload( null, false ); // user paging is not reset on reload
            }, 30000 );


        });
    </script>

@endsection