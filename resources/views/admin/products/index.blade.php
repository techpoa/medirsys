<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 4/30/2017
 * Time: 2:19 PM
 */

?>

@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="col-md-6">
                <h1>
                    Products
                </h1>
                <ol class="breadcrumb">
                    <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li><a href="#">Administrator</a></li>
                    <li class="active">Products</li>
                </ol>
            </div>
            <div class="col-md-6">
                <div class="title-action">
                    <a href="{{url('products/create')}}"  class="btn btn-sm btn-primary pull-right" > Create Product</a>
                </div>
            </div>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="activeProductsTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th> Name </th>
                                    <th> Category </th>
                                    <th> Purchase <br> Price </th>
                                    <th> Hire <br> Price </th>
                                    <th> Quantity </th>
                                    <th> Hire </th>
                                    <th> Sale </th>
                                    <th> Offers </th>
                                    <th> Status </th>
                                    <th> Last <br>Updated </th>
                                    <th> Action </th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th> Name </th>
                                    <th> Category </th>
                                    <th> Purchase <br> Price </th>
                                    <th> Hire <br> Price </th>
                                    <th> Quantity </th>
                                    <th> Hire </th>
                                    <th> Sale </th>
                                    <th> Offers </th>
                                    <th> Status </th>
                                    <th> Last <br> Updated </th>
                                    <th> Action </th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

    {{--MODALS --}}

    <div class="modal inmodal fade" id="editModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span></button>
                    <h4 class="modal-title"> Edit Product Details </h4>
                </div>
                <div class="modal-body">
                    <form id="frmEdit" role="form" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="col-md-6">
                            <label for="editName">Name</label>
                            <input type="text" class="form-control" id="editName" name="editName" placeholder="Enter Name">
                        </div>
                        <div class="col-md-6">
                            <label for="editCategory">Product Category</label>
                            <select class="form-control" id="editCategory" name="editCategory">
                                <option>-- Select Category --</option>
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="editModel">Model</label>
                            <input type="text" class="form-control" id="editModel" name="editModel" placeholder="Enter Model">
                        </div>
                        <div class="col-md-6">
                            <label for="editQuantity">Quantity</label>
                            <input type="text" class="form-control" id="editQuantity" name="editQuantity" placeholder="Enter Price">
                        </div>
                        <div class="col-md-6">
                            <label for="editPrice">Purchase Price</label>
                            <input type="text" class="form-control" id="editPrice" name="editPrice" placeholder="Enter Price">
                        </div>
                        <div class="col-md-6">
                            <label for="editHirePrice">Hire Price</label>
                            <input type="text" class="form-control" id="editHirePrice" name="editHirePrice" placeholder="Enter Price">
                        </div>
                        <div class="col-md-12">
                            <label for="editDescription">Product Description</label>
                            <textarea class="form-control" id="editDescription" name="editDescription" placeholder="Product Description" rows="5"></textarea>
                        </div>
                        <input type="hidden" id="productID">

                        <div class="form-group">
                            <input type="hidden" class="form-control">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnCloseEdit" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="button" id="btnSaveEdit" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal fade" id="offerModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="offerTitle"></h4>
                </div>
                <div class="modal-body">
                    <form id="frmOffer" role="form" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="col-md-12">
                            <label for="offer_price">Set Offer Price</label>
                            <input type="text" class="form-control" id="offer_price" name="offer_price">
                        </div>
                        <input type="hidden" id="offerID">

                        <div class="form-group">
                            <input type="hidden" class="form-control">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnCloseOffer" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="button" id="btnMakeOffer" class="btn btn-primary">Make Offer</button>
                </div>
            </div>
        </div>
    </div>


    @include('admin.partials.modals')

    <!-- Page-Level Scripts -->
    <script type='text/javascript' charset="utf-8">
        $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#editDescription').summernote({
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontNames', ['Arial', 'Arial Black', 'Comic Sans MS', 'Courier New']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']]
                ]
            });

            var entity = "Product";
            var activateModal = $('#activateModal');
            var editModal = $('#editModal');
            var frmEdit = document.getElementById("frmEdit");
            var deactivateModal = $('#deactivateModal');
            var deleteModal = $('#deleteModal');
            var offerModal = $('#offerModal');
            var frmOffer = document.getElementById("frmOffer");


            var activeProductsTable = $('#activeProductsTable').DataTable({
                pageLength: 10,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'Essays System Categories'},
                    {extend: 'pdf', title: 'Essays System Categories'},
                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                        }
                    }
                ],
                "ajax":  "/activeproducts",
                "columns": [
                    {data: 'name', name: 'name'},
                    {data: 'categoryName', name: 'categoryName'},
                    {data: 'price', name: 'price'},
                    {data: 'hire_price', name: 'hire_price'},
                    {data: 'quantity', name: 'quantity'},
                    {data: 'for_hire', name: 'for_hire'},
                    {data: 'for_sale', name: 'for_sale'},
                    {data: 'on_offer', name: 'on_offer'},
                    {data: 'status', name: 'status'},
                    {data: 'updated_at', name: 'updated_at'},
                    {data: 'actions', name: 'actions'}
                ]
            });

            $(document).on("click", ".edit", function () {

                var btnSaveEdit = $("#btnSaveEdit");
                var id = $(this).data('id');
                $.ajax({
                    type: "GET",
                    url: "products/"+id+"/edit",
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    document.getElementById("productID").value = id;
                                    document.getElementById("editName").value = data.name;
                                    document.getElementById("editCategory").value = data.category;
                                    $('#editDescription').summernote('code', data.description);
                                    document.getElementById("editPrice").value = data.price;
                                    document.getElementById("editHirePrice").value = data.hire_price;
                                    document.getElementById("editQuantity").value = data.quantity;
                                    $('#editModal').modal('show');
                                    break;
                                } else if (data.status === '01') {
                                    activeProductsTable.ajax.reload( null, false );
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $("#btnSaveEdit").click(function () {

                var name = $("#editName").val();
                var category = $("#editCategory").val();
                var description = $("#editDescription").val();
                var price = $("#editPrice").val();
                var hire_price = $("#editHirePrice").val();
                var quantity = $("#editQuantity").val();
                var id = $("#productID").val();
                $.ajax({
                    type: "PATCH",
                    url: "products/"+id,
                    data: {
                        id: id,
                        name: name,
                        category_id: category,
                        description: description,
                        price: price,
                        hire_price: hire_price,
                        quantity: quantity
                    },
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    $('#editModal').modal('hide');
                                    frmEdit.reset();
                                    activeProductsTable.ajax.reload( null, false );
                                    toastr.success('Success!', data.message);
                                } else if (data.status === '01') {
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });


            $(document).on('click', 'a.yesForSale', function () {

                var id = $(this).data('id'); // get the item ID
                $.ajax({
                    type: "POST",
                    url: "products/yesforsale",
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    activeProductsTable.ajax.reload( null, false );
                                    toastr.warning('Success', data.message);
                                } else if (data.status === '01') {
                                    activeProductsTable.ajax.reload( null, false );
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on('click', 'a.NoForSale', function () {

                var id = $(this).data('id'); // get the item ID
                $.ajax({
                    type: "POST",
                    url: "products/noforsale",
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    activeProductsTable.ajax.reload( null, false );
                                    toastr.success('Success', data.message);
                                } else if (data.status === '01') {
                                    activeProductsTable.ajax.reload( null, false );
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on('click', 'a.yesForHire', function () {

                var id = $(this).data('id'); // get the item ID
                $.ajax({
                    type: "POST",
                    url: "products/yesforhire",
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    activeProductsTable.ajax.reload( null, false );
                                    toastr.warning('Success', data.message);
                                } else if (data.status === '01') {
                                    activeProductsTable.ajax.reload( null, false );
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on('click', 'a.noForHire', function () {

                var id = $(this).data('id'); // get the item ID
                $.ajax({
                    type: "POST",
                    url: "products/noforhire",
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    activeProductsTable.ajax.reload( null, false );
                                    toastr.success('Success', data.message);
                                } else if (data.status === '01') {
                                    activeProductsTable.ajax.reload( null, false );
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on('click', 'a.activate', function () {

                var id = $(this).data('id'); // get the item ID
                var name = $(this).data('name'); // get the item name
                document.getElementById("activateID").value = id;
                $("#activateTitle").html("Activate " + entity);
                $("#activateNotification").html("Are you sure you want to activate " + entity + " <span class='text-danger'>" + name + "</span>");
            });

            $('#btnActivate').click(function () {

                var id = $("#activateID").val();
                $.ajax({
                    type: "POST",
                    url: "products/activate",
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    activateModal.modal('hide');
                                    activeProductsTable.ajax.reload( null, false );
                                    toastr.success('Success', data.message);
                                } else if (data.status === '01') {
                                    activeProductsTable.ajax.reload( null, false );
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on('click', 'a.deactivate', function () {

                var id = $(this).data('id'); // get the item ID
                var name = $(this).data('name'); // get the item name
                document.getElementById("deactivateID").value = id;
                $("#deactivateTitle").html("Deactivate " + entity);
                $("#deactivateNotification").html("Are you sure you want to deactivate " + entity + " <span class='text-danger'>" + name + "</span>");
            });

            $('#btnDeactivate').click(function () {

                var id = $("#deactivateID").val();
                $.ajax({
                    type: "POST",
                    url: "products/deactivate",
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    deactivateModal.modal('hide');
                                    activeProductsTable.ajax.reload( null, false );
                                    toastr.warning('Success', data.message);
                                } else if (data.status === '01') {
                                    activeProductsTable.ajax.reload( null, false );
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on('click', 'a.yesForOffer', function () {

                var id = $(this).data('id'); // get the item ID
                $.ajax({
                    type: "POST",
                    url: "products/yesforoffer",
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    activeProductsTable.ajax.reload( null, false );
                                    toastr.info('Success', data.message);
                                } else if (data.status === '01') {
                                    activeProductsTable.ajax.reload( null, false );
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on('click', 'a.noForOffer', function () {

                var id = $(this).data('id'); // get the item ID
                var name = $(this).data('name'); // get the item name
                document.getElementById("offerID").value = id;
                $("#offerTitle").html("Activate  "+ name +' '+ entity + " Offer");
            });

            $('#btnMakeOffer').click(function () {

                var id = $("#offerID").val();
                var offer_price = $("#offer_price").val();
                $.ajax({
                    type: "POST",
                    url: "products/noforoffer",
                    data: {id: id,offer_price: offer_price},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    offerModal.modal('hide');
                                    frmOffer.reset();
                                    activeProductsTable.ajax.reload( null, false );
                                    toastr.success('Success', data.message);
                                } else if (data.status === '01') {
                                    activeProductsTable.ajax.reload( null, false );
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on('click', 'a.delete', function () {

                var id = $(this).data('id'); // get the item ID
                var name = $(this).data('name'); // get the item name
                document.getElementById("deleteID").value = id;
                $("#deleteTitle").html("Delete " + entity);
                $("#deleteNotification").html("Are you sure you want to delete " + entity + " <span class='text-danger'>" + name + "</span>");
            });


            $('#btnDelete').click(function () {

                var id = $("#deleteID").val();
                $.ajax({
                    type: "DELETE",
                    url: "products/"+id,
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    deleteModal.modal('hide');
                                    activeProductsTable.ajax.reload( null, false );
                                    toastr.success('Success!', data.message);
                                } else if (data.status === '01') {
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });


        });
    </script>

@endsection