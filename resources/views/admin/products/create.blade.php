<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 4/30/2017
 * Time: 4:20 PM
 */
?>

@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="col-md-6">
                <h1>
                    Create Product
                </h1>
                <ol class="breadcrumb">
                    <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li><a href="{{url('products')}}">Products</a></li>
                    <li class="active">Create</li>
                </ol>
            </div>
            <div class="col-md-6">

            </div>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    @include('admin.partials.flash-message')
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Product Details</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" action="{{url('products')}}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" placeholder="Enter Product Name">
                                </div>
                                <div class="form-group">
                                    <label for="category">Product Category</label>
                                    <select class="form-control" id="category" name="category" value="{{ old('category') }}">
                                        <option>-- Select Category --</option>
                                        @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="model">Model</label>
                                    <input type="text" class="form-control" id="model" name="model" value="{{ old('model') }}" placeholder="Enter Product Model">
                                </div>
                                <div class="form-group">
                                    <label for="description">Product Description</label>
                                    <textarea class="form-control" id="summernote" name="description" value="{{ old('description') }}" placeholder="Product Description" rows="5"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="price">Purchase Price</label>
                                    <input type="text" class="form-control" id="price" name="price" value="{{ old('price') }}" placeholder="Enter Purchase Price">
                                </div>
                                <div class="form-group">
                                    <label for="hire_price">Hire Price</label>
                                    <input type="text" class="form-control" id="hire_price" name="hire_price" value="{{ old('hire_price') }}" placeholder="Enter Hire Price">
                                </div>
                                <div class="form-group">
                                    <label for="quantity">Quantity</label>
                                    <input type="text" class="form-control" id="quantity" name="quantity" value="{{ old('quantity') }}" placeholder="Enter Product Quantity">
                                </div>
                                <div class="form-group">
                                    <label for="image">Product Image</label>
                                    <p class="help-block"> Make sure the image is of ideal quality and and square in dimensions.</p>
                                    <input type="file" id="image" name="image">
                                </div>
                                <div class="form-group">
                                    <label for="brochure">Equipment Brochure</label>
                                    <p class="help-block"> Make sure the file is of pdf extension.</p>
                                    <input type="file" id="brochure" name="brochure">
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.box -->

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

    {{--MODALS --}}

    @include('admin.partials.modals')

    <!-- Page-Level Scripts -->
    <script type='text/javascript' charset="utf-8">
        $(document).ready(function(){


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#summernote').summernote();

        });
    </script>

@endsection

