<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 5/6/2017
 * Time: 1:26 PM
 */
?>

@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="col-md-6">
                <h1>
                    Edit Product
                </h1>
                <ol class="breadcrumb">
                    <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li><a href="{{url('products')}}">Products</a></li>
                    <li class="active">Edit {{ $product -> name }} Details</li>
                </ol>
            </div>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                @include('admin.partials.flash-message')
                <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Product Details</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" action="{{'/products/'.$product->id}}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="PATCH">

                            <div class="box-body">

                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" id="name" name="name" value="{{ $product -> name }}">
                                </div>
                                <div class="form-group">
                                    <label for="category">Product Category</label>
                                    <select class="form-control" id="category" name="category_id" value="{{ $product -> category_id }}">
                                        <option value=""> -- Select Category -- </option>
                                        @foreach($categories as $category)
                                            @if($product -> category_id == $category->id)
                                                <option selected="selected" value="{{$category->id}}">{{$category->name}}</option>
                                             @else
                                              <option value="{{$category->id}}">{{$category->name}}</option>
                                             @endif   
                                            
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="model">Model</label>
                                    <input type="text" class="form-control" id="model" name="model" value="{{ $product -> model }}">
                                </div>
                                <div class="form-group">
                                    <label for="summernote">Product Description</label>
                                    <textarea class="form-control" id="summernote" name="description" rows="5">{!! html_entity_decode($product -> description) !!}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="price">Purchase Price</label>
                                    <input type="text" class="form-control" id="price" name="price" value="{{ $product -> price }}">
                                </div>
                                <div class="form-group">
                                    <label for="hire_price">Hire Price</label>
                                    <input type="text" class="form-control" id="hire_price" name="hire_price" value="{{ $product -> hire_price }}">
                                </div>
                                <div class="form-group">
                                    <label for="quantity">Quantity</label>
                                    <input type="text" class="form-control" id="quantity" name="quantity" value="{{ $product -> quantity }}">
                                </div>
                                <div class="form-group">
                                    <label for="file">Product Image</label>
                                    <p class="help-block"> Make sure the image is of ideal quality and and square in dimensions.</p>
                                    <input type="file" id="file" name="file">
                                </div>
                                <div class="form-group">
                                    <label for="brochure">Equipment Brochure</label>
                                    <p class="help-block"> Make sure the file is of pdf extension.</p>
                                    <input type="file" id="brochure" name="brochure">
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

    {{--MODALS --}}

    @include('admin.partials.modals')

    <!-- Page-Level Scripts -->
    <script type='text/javascript' charset="utf-8">
        $(document).ready(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });
    </script>

@endsection

