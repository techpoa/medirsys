<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 5/8/2017
 * Time: 2:39 PM
 */

?>

@extends('layouts.master')
@section('content')
    <!-- BEGIN: PAGE CONTAINER -->
    <div class="c-layout-page">
        <!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
        <div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
            <div class="container">
                <div class="c-page-title c-pull-left">
                    <h3 class="c-font-uppercase c-font-sbold">Checkout</h3>
                </div>
                <ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
                    <li><a href="{{url('products')}}">Hire</a></li>
                    <li>/</li>
                    <li><a href="{{url('cart')}}">Cart</a></li>
                    <li>/</li>
                    <li class="c-state_active">Checkout </li>
                </ul>
            </div>
        </div>
        <!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
    @include('admin.partials.flash-message')
    <!-- BEGIN: PAGE CONTENT -->
        <div class="c-content-box c-size-lg">
            <div class="container">
                <div class="row">
                    <!-- BEGIN: ADDRESS FORM -->
                    <div class="col-md-7 c-padding-20">
                        <!-- BEGIN: BILLING ADDRESS -->
                        <h3 class="c-font-bold c-font-uppercase c-font-24">Order Details</h3>
                        <form role="form" action="{{url('../cart/posthirecheckout')}}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <fieldset id="pay_details">
                                <h4 class="c-font-bold c-font-uppercase c-font-24">Payment Details</h4>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <ol class="c-links c-theme-ul">
                                            <li><a href="#">Access your Safaricom menu, then select <strong>M-PESA.</strong></a></li>
                                            <li><a href="#">Choose Lipa na M-PESA, then <strong>Pay Bill.</strong> </a></li>
                                            <li><a href="#">Enter Medir Systems Mpesa Pay bill number,<strong>697483.</strong></a></li>
                                            <li><a href="#">Enter your ID Number</a></li>
                                            <li><a href="#">Next, enter the amount to pay.</a></li>
                                            <li><a href="#">M-PESA asks you for your M-Pesa PIN. Enter your PIN.</a></li>
                                            <li><a href="#">Double check and confirm that you have entered the right information
                                                    and hit OK to send and complete the M-PESA transaction</a></li>
                                            <li><a href="#">You now receive a transaction confirmation SMS from M-PESA..</a></li>
                                        </ol>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label class="control-label" for="transaction_number"> Mpesa Transaction Number</label>
                                        <input type="text" class="form-control c-square c-theme"  id="transaction_number" name="transaction_number" placeholder="Transaction Number">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group pull-right" role="group">
                                        <button type="button" id="nextButton" class="btn btn-sm c-theme-btn c-btn-square c-btn-bold">Next</button>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset id="order_details" class="hidden">
                                <h4 class="c-font-bold c-font-uppercase c-font-24">Order Details</h4>
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label class="control-label">Full Names</label>
                                        <input type="text" class="form-control c-square c-theme" name="name" placeholder="First Name">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="control-label">Email Address</label>
                                        <input type="email" class="form-control c-square c-theme" name="email" placeholder="Email Address">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="control-label">Phone</label>
                                        <input type="tel" class="form-control c-square c-theme" name="phone" placeholder="Phone" data-inputmask='"mask": "(999) 999-9999"' data-mask>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label class="control-label">Address</label>
                                        <input type="text" class="form-control c-square c-theme" name="address" placeholder="Address">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label class="control-label">Date From for Hire</label>
                                        <input type="text" class="form-control c-square c-theme datepicker" name="start_date" placeholder="Address">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label" for="order_type">Date To for Hire</label>
                                        <input type="text" class="form-control c-square c-theme datepicker" name="end_date" placeholder="Address">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label class="control-label">Order Notes</label>
                                        <textarea class="form-control c-square c-theme" rows="3" name="details" placeholder="Note about your order, e.g. special notes for delivery."></textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6 pull-right" role="group">
                                        <button type="button" class="btn btn-lg btn-default c-btn-square c-btn-uppercase c-btn-bold">Cancel</button>
                                        <button type="submit" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Submit</button>
                                    </div>
                                </div>
                            </fieldset>

                        </form>
                    </div>
                    <!-- END: ADDRESS FORM -->
                    <!-- BEGIN: ORDER FORM -->
                    <div class="col-md-5">
                        <div class="c-content-bar-1 c-align-left c-bordered c-theme-border c-shadow">
                            <h1 class="c-font-bold c-font-uppercase c-font-24">Your Order</h1>
                            <ul class="c-order list-unstyled">
                                <li class="row c-margin-b-15">
                                    <div class="col-md-6 c-font-20">
                                        <h2>Product</h2>
                                    </div>
                                    <div class="col-md-6 c-font-20">
                                        <h2>Total</h2>
                                    </div>
                                </li>
                                <li class="row c-border-bottom"></li>
                                @foreach ($contents as $item)
                                    <li class="row c-margin-b-15 c-margin-t-15">
                                        <div class="col-md-6 c-font-20">
                                            <a href="{{url('product/details',$item->id)}}" class="c-theme-link">{{ $item->name }} x {{ $item->qty }}</a>
                                        </div>
                                        <div class="col-md-6 c-font-20">
                                            <p class="">{{ $item->subtotal }}</p>
                                        </div>
                                    </li>
                                @endforeach
                                <li class="row c-margin-b-15 c-margin-t-15">
                                    <div class="col-md-6 c-font-20">Subtotal</div>
                                    <div class="col-md-6 c-font-20">
                                        <p class="">Ksh
                                            <span class="c-subtotal">{{ Cart::instance('hire')->subtotal() }}</span>
                                        </p>
                                    </div>
                                </li>
                                <li class="row c-border-top c-margin-b-15"></li>
                                <li class="row c-margin-b-15 c-margin-t-15">
                                    <div class="col-md-6 c-font-20">
                                        <p class="c-font-30">Total</p>
                                    </div>
                                    <div class="col-md-6 c-font-20">
                                        <p class="c-font-bold c-font-30">Ksh
                                            <span class="c-shipping-total">Ksh {{ Cart::instance('hire')->subtotal() }}</span>
                                        </p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- END: ORDER FORM -->
                </div>
            </div>
        </div>
        <!-- END: PAGE CONTENT -->
    </div>
    <!-- END: PAGE CONTAINER -->

    <script type='text/javascript' charset="utf-8">
        $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $("#nextButton").click(function() {
                $("#pay_details").addClass("hidden");
                $("#order_details").removeClass("hidden");
            });

            $('.datepicker').datepicker({
                autoclose: true
            });

            var frmCheckout = document.getElementById("frmCheckout");

            $("#btnCheckout").click(function () {

                var frm = $('#frmCheckout');
                var data = frm.serialize();
                $.ajax({
                    type: "POST",
                    url: "../cart/checkout",
                    data: data,
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    window.location.href = '{{ url('/cart/checkout') }}'
//                                frmCheckout.reset();
//                                toastr.success('Success!', data.message);
//                                location.reload();
                                } else if (data.status === '01') {
//                                toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
//                            toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

        });
    </script>

@endsection
