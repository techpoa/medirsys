<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('mg.devinvent.com'),
        'secret' => env('k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCyMvJyqgfeqaQkO02kLq8rQNhwX/IfMg+gVSgNkvIA9UVxaDrNr041/dp4DlGAh48qvyKAZ06aNuwzzQwkTEns7jrgFfEz4oZlSuQ7Q45SlF3fzxsXdHAgwNtdjPg4JdB3PSieXIKXdD56mrysa1wEuBZ7zbE8KYEyDglkn7yxhwIDAQAB'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

];
