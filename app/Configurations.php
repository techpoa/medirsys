<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Configurations extends Model
{
    //
    protected $table = 'mdr_configurations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'main_address', 'other_address', 'main_phone', 'other_phone', 'admin_email', 'created_at',
    ];
}
