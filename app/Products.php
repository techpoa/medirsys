<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Products extends Model
{
//    use SoftDeletes;

    //
    protected $table = 'mdr_products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id', 'name', 'model', 'description', 'price', 'discount_price', 'hire_price', 'hire_discount', 'quantity', 'image',
        'for_hire',
        'for_sale',
        'on_offer',
        'offer_price',
        'status',
        'created_at',
    ];

//    protected $dates = ['deleted_at'];

}
