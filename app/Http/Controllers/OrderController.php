<?php

namespace App\Http\Controllers;

use App\Orders;
use Carbon\Carbon;
use Gloudemans\Shoppingcart\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;

class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.orders.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $Order = Orders::findOrFail($id);

        if ($Order->delete())
        {
            return response()->json(['status' => '00', 'message' => 'Order details has been Deleted Successfully']);
        }
        else{
            return response()->json(['status' => '01', 'message' => 'We have had an error processing your request']);
        }
    }


    public function activeOrders()
    {
        $orders = DB::table('mdr_orders')->get();

        return Datatables::of($orders)
            ->editColumn('actions', function ($order) {
                if ($order->status == 1) return '

                <a href="#" type="button" class="btn btn-success btn-xs activate" data-toggle="modal" data-id="'.$order->id .'" 
                data-name="'.$order->order_number .'" title="Mark Accepted" data-target="#activateModal" > <i class="fa fa-thumbs-up"></i></a>
                
                <a href="#" type="button" class="btn btn-warning btn-xs deactivate" data-toggle="modal" data-id="'.$order->id .'"   
                data-name="'.$order->order_number .'" title="Decline Order" data-target="#deactivateModal" > <i class="fa fa-thumbs-down"></i></a>
                
                <a href="#" type="button" class="btn btn-danger btn-xs delete"  data-toggle="modal" data-id="'.$order->id .'" 
                 data-name="'.$order->order_number .'" title="Delete Order" data-target="#deleteModal"> <i class="fa fa-trash-o"></i> </a>
                
                ';
                else
                    return '<a href="#" type="button" class="btn btn-danger btn-xs delete"  data-toggle="modal" data-id="'.$order->id .'" 
                 data-name="'.$order->order_number .'" title="Delete Order" data-target="#deleteModal"> <i class="fa fa-trash-o"></i> </a>';
            })
            ->rawColumns(['actions', 'actions'])
            ->make(true);

    }

    public function activeOrderDetails()
    {

    }

    public function orderDetailsData(Request $request)
    {
        $order_id = $request -> input('id');
        $payments = DB::table('mdr_order_details')
            ->join('mdr_products','mdr_order_details.product_id','=','mdr_products.id')
            ->where('mdr_order_details.order_id', $order_id)
            ->select('mdr_order_details.*','mdr_products.name as product')
            ->get();

        return Datatables::of($payments)
            ->make(true);
    }

    public function accept(Request $request){

        $order_id = $request ->input('id');

        DB::table('mdr_orders')
            ->where('id', $order_id)
            ->update(['status' => 2]);

        return response()->json(['status' => '00', 'message' => 'Order has been Accepted Successfully']);

    }

    public function decline(Request $request){

        $order_id = $request ->input('id');

        DB::table('mdr_orders')
            ->where('id', $order_id)
            ->update(['status' => 3]);

        return response()->json(['status' => '00', 'message' => 'Order has been Declined Successfully']);

    }
}
