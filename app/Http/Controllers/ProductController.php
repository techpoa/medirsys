<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Products;
use App\Traits\FormatAjaxValidationMessages;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Exception\NotReadableException;
use Intervention\Image\Facades\Image;
use Yajra\Datatables\Datatables;

class ProductController extends Controller
{
    use FormatAjaxValidationMessages;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categories = DB::table('mdr_product_categories')->get();
        return view('admin.products.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Categories::all();
        return view('admin.products.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'category' => 'required',
            'name' => 'required',
            'price' => 'required|numeric',
            'hire_price' => 'numeric',
            'quantity' => 'required|numeric',
        ]);
        //
        if($validator -> passes())
        {
            $products = new Products();

            $products->category_id = $request->input('category');
            $products->name = $request->input('name');
            $products->models = $request->input('model');
            $products->description = $request->input('description');
            $products->price = $request->input('price');
            $products->hire_price = $request->input('hire_price');
            $products->quantity = $request->input('quantity');
            $products->for_sale = $request->input('for_sale');
            $products->for_hire = $request->input('for_hire');
            $products->status = 2;
            $products->created_at = Carbon::now();

            $main_pic = $request->file('image');
            $input['main_pic_name'] = time().'.'.$main_pic->getClientOriginalExtension();
            $main_pic_path = 'uploads/products';
            $main_pic_img = Image::make($main_pic->getRealPath());
            $main_pic_img->resize(425, 425)->save($main_pic_path.'/'.$input['main_pic_name']);

            $products->image = $main_pic_path.'/'.$input['main_pic_name'];

            $thumbnail_pic= $request->file('image');
            $input['thumbnail_pic_name'] = time().'.'.$thumbnail_pic->getClientOriginalExtension();
            $thumbnail_pic_path = 'uploads/products/thumbnail';
            $thumbnail_pic_img = Image::make($thumbnail_pic->getRealPath());
            $thumbnail_pic_img->resize(100, 100)->save($thumbnail_pic_path.'/'.$input['thumbnail_pic_name']);

            $products->thumbnail = $thumbnail_pic_path.'/'.$input['thumbnail_pic_name'];


            $brochure = $request->file('brochure');
            $brochure_path = 'uploads/brochure';
            if(!empty($brochure))
            {
                $input['brochure_name'] = time().'.'.$brochure->getClientOriginalExtension();
                $brochure->move($brochure_path, $input['brochure_name']);
                $products->brochure = $brochure_path.'/'.$input['brochure_name'];
            }


            if ($products->save())
            {
                return back()->withInput()->with('success','Created Product Successfully');
            }
            else
            {
                return back()->withInput()->with('error','Error Creating Product');
            }
        }
        else{
            return Redirect::back()->withErrors($validator);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        //
        $product = Products::find($id);
        return view('show')->with('product', $product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        //
        $product = Products::where('id', $id)->firstOrFail();

        $categories = Categories::all();

        return view('admin.products.edit',compact('product','categories'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'name' => 'required',
            'price' => 'required|numeric',
            'hire_price' => 'numeric',
            'quantity' => 'required|numeric',
        ]);
        //
        if($validator -> passes())
        {
            $product = Products::find($id);
            $input = $request->all();


            $image = $request->file('file');
            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

            if(!empty($image))
            {

                $thumbnailPath = public_path('uploads/products/thumbnail');
                $img = Image::make($image->getRealPath());
                $img->resize(100, 100, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($thumbnailPath.'/'.$input['imagename']);
                $product->thumbnail = $thumbnailPath.'/'.$input['imagename'];

                $imagePath = public_path('uploads/products');
                $image->move($imagePath, $input['imagename']);

                $product->image = $imagePath.'/'.$input['imagename'];
            }


            $brochure = $request->file('brochure');
            $input['brochurename'] = time().'.'.$image->getClientOriginalExtension();
            if (!empty($input['brochurename']))
            {
                $brochurePath = public_path('uploads/brochures');
                $brochure->move($brochurePath, $input['brochurename']);

                $product->brochure = $brochurePath.'/'.$input['brochurename'];
            }

            $product->fill($input)->save();

            return back()->with('success', 'Product Details changes have been updated Successfully.');
        }
        else{
            return Redirect::back()->withErrors($validator);
        }



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $order_products = DB::table('mdr_order_details')
            ->where('product_id',$id)
            ->get();

        if ($order_products->isEmpty())
        {
            $product = Products::findOrFail($id);
            $product->delete();

            return response()->json(['status' => '00', 'message' => 'Product details has been Deleted Successfully']);
        }
        else{
            return response()->json(['status' => '01', 'message' => 'The Product has associated order records -- Cannot be deleted']);
        }
    }

    public function activeProducts()
    {

//                <a href="#" type="button" class="btn btn-primary btn-xs edit" data-toggle="modal" data-id="'.$product->id .'"
//                data-name="'.$product->name .'" title="Edit Product" data-target="#editModal" > <i class="fa fa-edit"></i></a>

        $products = DB::table('mdr_products')
            ->join('mdr_product_categories', 'mdr_products.category_id', '=', 'mdr_product_categories.id')
            ->select('mdr_products.*', 'mdr_product_categories.name as categoryName')
            ->get();

        return Datatables::of($products)
            ->editColumn('for_hire', function ($product) {
                if ($product->for_hire == 1) return '<a href="#" type="button" class="btn btn-success btn-xs  yesForHire" data-id="'.$product->id .'"> Yes </a>';
                if ($product->for_hire != 1) return '<a href="#" type="button" class="btn btn-warning btn-xs noForHire" data-id="'.$product->id .'"> No </i></a>';
                return 'Cancel';
            })
            ->editColumn('for_sale', function ($product) {
                if ($product->for_sale == 1) return '<a href="#" type="button" class="btn btn-success btn-xs yesForSale" data-id="'.$product->id .'"  
                data-name="'.$product->name .'" title="Hire Product" > Yes </a>';
                if ($product->for_sale != 1) return '<a href="#" type="button" class="btn btn-warning btn-xs NoForSale" data-id="'.$product->id .'"  
                data-name="'.$product->name .'" title="Hire Product" > No </i></a>';
                return 'Cancel';
            })
            ->editColumn('on_offer', function ($product) {
                if ($product->on_offer == 1) return '<a href="#" type="button" class="btn btn-success btn-xs yesForOffer" data-id="'.$product->id .'"  
                data-name="'.$product->name .'" title="Click to Cancel Offer" > Yes </a>';
                if ($product->on_offer != 1) return '<a href="#" type="button" class="btn btn-info btn-xs noForOffer" data-toggle="modal" data-id="'.$product->id .'"  
                data-name="'.$product->name .'" title="Activate Item Offer" data-target="#offerModal" > No </a>';
                return 'Cancel';
            })
            ->editColumn('status', function ($product) {
                if ($product->status == 1) return '
                    <a href="#" type="button" class="btn btn-success btn-xs deactivate" data-toggle="modal" data-id="'.$product->id .'" data-name="'.$product->name .'" title="Deactivate Product" data-target="#deactivateModal" > 
                Active</a>';
                if ($product->status != 1) return '<a href="#" type="button" class="btn btn-warning btn-xs activate" data-toggle="modal" data-id="'.$product->id .'" data-name="'.$product->name .'" title="Activate Product" data-target="#activateModal" > Inactive </a>';
                return 'Cancel';
            })
            ->editColumn('actions', function ($product) {
                if ($product->status == 1) return '
                <div class="btn-group">

                
                <a href="/products/' . $product->id . '/edit" title="Edit Product" class="btn btn-info btn-xs edit" data-id="' . $product->id . '">
                <i class="fa fa-edit"></i></a>
                
                
                <a href="#" type="button" class="btn btn-danger btn-xs delete" data-toggle="modal" data-id="'.$product->id .'" 
                data-name="'.$product->name .'" title="Delete Product" data-target="#deleteModal" > <i class="fa fa-trash-o"></i></a>
                </div>
                ';
                if ($product->status != 1) return '
                <div class="btn-group">
                                
                <a href="/products/' . $product->id . '/edit" title="Edit Product" class="btn btn-info btn-xs edit" data-id="' . $product->id . '">
                <i class="fa fa-edit"></i></a>
                    
                
                <a href="#" type="button" class="btn btn-danger btn-xs delete" data-toggle="modal" data-id="'.$product->id .'" 
                data-name="'.$product->name .'" title="Delete Product" data-target="#deleteModal" > <i class="fa fa-trash-o"></i></a>
                </div>
                ';
                return 'Cancel';
            })
            ->rawColumns(['for_hire','for_sale', 'on_offer', 'status', 'actions'])
            ->make(true);

    }

    public function activate(Request $request){

        $user_id = $request ->input('id');

        DB::table('mdr_products')
            ->where('id', $user_id)
            ->update(['status' => 1]);

        return response()->json(['status' => '00', 'message' => 'Product has been Activated Successfully']);

    }

    public function deactivate(Request $request){

        $user_id = $request ->input('id');

        DB::table('mdr_products')
            ->where('id', $user_id)
            ->update(['status' => 2]);

        return response()->json(['status' => '00', 'message' => 'Product has been Deactivated Successfully']);

    }

    public function yesForHire(Request $request)
    {
        DB::table('mdr_products')
            ->where('id', $request ->input('id'))
            ->update(['for_hire' => 2]);

        return response()->json(['status' => '00', 'message' => 'Product Disabled for Hire']);
    }

    public function noForHire(Request $request)
    {
        DB::table('mdr_products')
            ->where('id', $request ->input('id'))
            ->update(['for_hire' => 1]);

        return response()->json(['status' => '00', 'message' => 'Product Marked for hire']);
    }

    public function yesForSale(Request $request)
    {
        DB::table('mdr_products')
            ->where('id', $request ->input('id'))
            ->update(['for_sale' => 2]);

        return response()->json(['status' => '00', 'message' => 'Product Disabled for Sale']);
    }

    public function noForSale(Request $request)
    {
        DB::table('mdr_products')
            ->where('id', $request ->input('id'))
            ->update(['for_sale' => 1]);

        return response()->json(['status' => '00', 'message' => 'Product Marked for sale']);
    }

    public function yesForOffer(Request $request)
    {
        DB::table('mdr_products')
            ->where('id', $request ->input('id'))
            ->update(['on_offer' => 2]);

        return response()->json(['status' => '00', 'message' => 'Product Disabled From Offers']);
    }

    public function noForOffer(Request $request)
    {

        DB::table('mdr_products')
            ->where('id', $request ->input('id'))
            ->update(['on_offer' => 1,'offer_price' => $request ->input('offer_price')]);


        return response()->json(['status' => '00', 'message' => 'Product Enabled for Offer at '.$request->input('offer_price')]);
    }


}
