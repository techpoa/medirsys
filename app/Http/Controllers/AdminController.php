<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        //
        $products = DB::table('mdr_products')->count();

        $for_hire = DB::table('mdr_products')->where('for_hire','=',1)->count();

        $for_sale = DB::table('mdr_products')->where('for_sale','=',1)->count();

        $orders = DB::table('mdr_orders')->count();

        $pending_orders = DB::table('mdr_orders')->where('status','=',1)->count();

        $subscriptions = DB::table('mdr_subscriptions')->count();

        $users = DB::table('mdr_users')->count();

        $chart_sales = DB::table('mdr_orders')
            ->select(DB::raw('SUM(total) as total, MONTH(created_at) as month'))
            ->where('status','=',2)
            ->whereYear('created_at', '=', Carbon::now()->toDateString())
            ->groupBy("month")
            ->get()
            ->toArray();

        $chart_sales_months = array();
        $chart_sales_total = array();

        foreach ($chart_sales as $key => $chart_sale) {
            $chart_sales_months[$key] =  date("F", mktime(0, 0, 0, $chart_sale->month, 1));
            $chart_sales_total[$key] =  $chart_sale->total;
        }


        $chart_orders = DB::table('mdr_orders')
            ->select(DB::raw('COUNT(total) as total, MONTH(created_at) as month'))
            ->where('status','=',2)
            ->whereYear('created_at', '=', Carbon::now()->toDateString())
            ->groupBy("month")
            ->get()
            ->toArray();


        $chart_orders_months = array();
        $chart_orders_total = array();

        foreach ($chart_orders as $key => $chart_order) {
            $chart_orders_months[$key] =  $chart_order->month;
            $chart_orders_total[$key] =  $chart_order->total;
        }

//        $asset_worth $all_orders $complete_orders $this_month_revenue
        $asset_worth = DB::table('mdr_products')
            ->where('status','=',1)
            ->sum('price');

        $all_orders = DB::table('mdr_orders')
            ->where('status','=',1)
            ->sum('total');

        $complete_orders = DB::table('mdr_orders')
            ->where('status','=',1)
            ->sum('total');

        $this_month_revenue = DB::table('mdr_orders')
            ->whereMonth('created_at', '=', Carbon::now()->toDateString())
            ->where('status','=',2)
            ->sum('total');


        return view('admin.index')
            ->with('products',$products)
            ->with('for_hire',$for_hire)
            ->with('for_sale',$for_sale)
            ->with('orders',$orders)
            ->with('pending_orders',$pending_orders)
            ->with('subscriptions',$subscriptions)
            ->with('users',$users)
            ->with('asset_worth',$asset_worth)
            ->with('all_orders',$all_orders)
            ->with('complete_orders',$complete_orders)
            ->with('this_month_revenue',$this_month_revenue)
            ->with('chart_sales_months',json_encode($chart_sales_months,JSON_NUMERIC_CHECK))
            ->with('chart_sales_total',json_encode($chart_sales_total,JSON_NUMERIC_CHECK))
            ->with('chart_orders_months',json_encode($chart_orders_months,JSON_NUMERIC_CHECK))
            ->with('chart_orders_total',json_encode($chart_orders_total,JSON_NUMERIC_CHECK));
    }

    public function profile()
    {
        $user = Auth::user();
        return view('admin.users.profile',compact('user'));
    }

    public function postProfile(Request $request)
    {



        $User = User::find(Auth::id());

        $User->name = $request->input('name');
        $User->sname = $request->input('sname');
        $User->email = $request->input('email');
        $User->phone = $request->input('phone');
        $User->created_at = Carbon::now();

        if(!empty($request -> file('file')))
        {
            $image = $request->file('file');
            $input['image_name'] = time().'.'.$image->getClientOriginalExtension();
            $image_path = 'uploads/avatars';
            $image_img = Image::make($image->getRealPath());
            $image_img->resize(425, 425)->save($image_path.'/'.$input['image_name']);

            $User->avatar = $image_path.'/'.$input['image_name'];
        }

        $User->save();

        return back()->with('success','Profile updated successfully!');

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function password()
    {
        return view('admin.users.password');
    }

    public function postPassword(Request $request)
    {
        $current = $request->input('current_password');
        $new = $request->input('new_password');
        $confirm = $request->input('confirm_password');
        if(Hash::check($current, Auth::user()->password))
        {
            if($new == $confirm)
            {
                DB::table('mdr_users')
                    ->where('id', Auth::id())
                    ->update(['password' => Hash::make($new)]);

                return back()->with('success','Password updates successfully!');
            }
            else
            {
                return back()->with('error','Your Confirmation password does not match');
            }
        }
        else
        {
            return back()->with('error','Your current password does not match');
        }
    }
}
