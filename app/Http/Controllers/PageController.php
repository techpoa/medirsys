<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Contacts;
use App\Products;
use Carbon\Carbon;
use Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categories = Categories::all();
        $featured = DB::table('mdr_products')->get()->random();
        $contents = Cart::content();
        $items = Products::get()->random(6);
        $offers = Products::where('on_offer','=',1)->get();

        return view('index',compact('categories','featured','items','contents','offers'));

    }

    public function about()
    {
        $contents = Cart::content();
        return view('about',compact('contents','items'));

    }

    public function product(Request $request)
    {

        $contents = Cart::content();
        $categories = DB::table('mdr_product_categories')->get();
        $products = DB::table('mdr_products')->where('status','=',1)->where('for_sale','=',1)->simplePaginate(9);
        $offers = Products::where('on_offer','=',1)->get();
        return view('products',compact('products','contents','items','offers','categories'));
    }

    public function filterProduct(Request $request)
    {
//        dd($request->input('category'));
        $productCategory = $request->input('category');
        $productSort = $request->input('sort');
        $productItems = $request->input('items');

//        $products = Products::where('status','=',1)->where('for_sale','=',1)->simplePaginate(9);
        $contents = Cart::content();
        $categories = DB::table('mdr_product_categories')->get();
        $offers = Products::where('on_offer','=',1)->get();
        if (!empty($productCategory))
            $products = Products::where('status','=',1)
                ->where('for_sale','=',1)
                ->where('category_id','=',$productCategory)
                ->simplePaginate(9);
        if (!empty($productSort))
            $products = Products::where('status','=',1)->where('for_sale','=',1)
                ->orderBy('name', $productSort)
                ->simplePaginate(9);
        if (!empty($productItems))
            $products = Products::where('status','=',1)->where('for_sale','=',1)->simplePaginate($productItems);


        return view('products',compact('products','contents','items','offers','categories'));

    }

    public function details($id)
    {
        $contents = Cart::content();
        $product = Products::where('id', $id)->firstOrFail();
        $items = Products::where('id', '!=', $id)->where('status','=',1)->get()->random(4);
        return view('details',compact('product','items','contents'));
    }

    public function hiredetails($id)
    {
        $contents = Cart::content();
        $product = Products::where('id', $id)->firstOrFail();
        $items = Products::where('id', '!=', $id)->where('status','=',1)->get()->random(4);
        return view('hiredetails',compact('product','items','contents'));
    }

    public function hire()
    {
        $contents = Cart::content();
        $products = DB::table('mdr_products')->where('status','=',1)->where('for_hire','=',1)->simplePaginate(9);
        $categories = DB::table('mdr_product_categories')->get();
        $offers = Products::where('on_offer','=',1)->get();
        return view('hire',compact('products','contents','offers','categories'));
    }

    public function filterHire(Request $request)
    {
        $productCategory = $request->input('category');

        $contents = Cart::content();
        $categories = DB::table('mdr_product_categories')->get();
        $offers = Products::where('on_offer','=',1)->get();
        if (!empty($productCategory))
            $products = Products::where('status','=',1)
                ->where('for_hire','=',1)
                ->where('category_id','=',$productCategory)
                ->simplePaginate(9);
        if (!empty($productSort))
            $products = Products::where('status','=',1)->where('for_hire','=',1)
                ->orderBy('name', $productSort)
                ->simplePaginate(9);
        if (!empty($productItems))
            $products = Products::where('status','=',1)->where('for_hire','=',1)->simplePaginate($productItems);


        return view('products',compact('products','contents','items','offers','categories'));
    }

    public function services()
    {
        $contents = Cart::content();
        return view('services',compact('contents'));
    }

    public function contacts()
    {
        $contents = Cart::content();
        $products = DB::table('mdr_products')
            ->where('status','=',1)
            ->where('thumbnail','!=','')
            ->get();
        return view('contacts',compact('contents','products'));
    }

    public function postContacts(Request $request)
    {
        $Contact = new Contacts();

        $Contact->name = $request->input('name');
        $Contact->email = $request->input('email');
        $Contact->phone = $request->input('phone');
        $Contact->message = $request->input('message');
        $Contact->created_at = Carbon::now();

        // notification
        DB::table('mdr_notifications')->insert([
            [
                'title' => "Contact Information",
                'message' => "A contact message had been dropped by the website by ".$request->input('name'),
                'status' => 1,
                'created_at' => Carbon::now()
            ]
        ]);


        if ($Contact->save())
        {
            return back()->with('success','Your mail has been sent Successfully');
        }
        else
        {
            return back()->with('error','We have encountered an Error handling youren request . Please try again');
        }
    }



    public function downloadBrochure(Request $request)
    {
        $productId = $request -> input('id');
        $productDetails = DB::table('mr_products')
            ->where('id',$productId)
            ->first();


        //PDF file is stored under project/public/download/info.pdf
        $file= public_path()+$productDetails->brochure;

        $headers = array(
            'Content-Type: application/pdf',
        );

        return Response::download($file, 'filename.pdf', $headers);
    }


}
