<?php

namespace App\Http\Controllers;

//use Gloudemans\Shoppingcart\Cart;
use App\Orders;
use App\Products;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

    $contents = Cart::instance('default')->content();
        $items = Products::where('for_sale','=',1)->get();
//        dd($contents);
        return view('cart',compact('contents','items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
//        Cart::associate('Product','App')->add($request->id, $request->name, 1, $request->price);

        Cart::add($request->id, $request->name, 1, $request->price);
//        Cart:associate($cartItem->rowId, \App\Product::class);

        return redirect('cart')->withSuccessMessage('Item was added to your cart!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Cart::instance('default')->remove($id);
        return redirect('cart')->withSuccessMessage('Item has been removed!');
    }

    public function getCheckout()
    {
        $contents = Cart::instance('default')->content();
//        dd($contents);
        return view('checkout',compact('contents'));
    }

    public function postCheckout(Request $request)
    {

        $items = Cart::instance('default')->content();

//        dd(Cart::subtotal(2));

        $id = DB::table('mdr_orders')->insertGetId(
            [
                'order_number' => $this -> getOrderNumber(),
                'order_type' => 'Sale',
                'name' => $request->input('name'),
                'address' => $request->input('address'),
                'email' => $request->input('email'),
                'phone' => $request->input('phone'),
                'details' => $request->input('details'),
                'total' =>number_format(str_replace(",", "", Cart::subtotal()), 2, '.', ''),
                'status' => 1,
                'created_at' => Carbon::now()
            ]
        );


        // push order details
        foreach ($items as $item)
        {
            $order_details =  DB::table('mdr_order_details')->insert([
                [
                    'order_id' => $id,
                    'product_id' => $item -> id,
                    'quantity' => $item -> qty,
                    'price' => $item -> price
                ]
            ]);
        }

        Cart::instance('default')->destroy();


        return back()->with('success','Created Order Successfully :: You will get an email confirmation when your order is accepted');

    }

    public function getHireCart()
    {
        $contents = Cart::instance('hire')->content();
        $items = Products::where('for_hire','=',1)->where('status','=',1)->get();
        return view('hirecart',compact('contents','items'));
    }

    public function postHireCart(Request $request)
    {
        Cart::instance('hire')->add($request->id, $request->name, 1, $request->price);
//        Cart:associate($cartItem->rowId, \App\Product::class);

        return redirect('cart/hire')->withSuccessMessage('Item was added to your hire cart!');
    }

    public function getHireCheckout()
    {
        $contents = Cart::instance('hire')->content();
        return view('hirecheckout',compact('contents'));
    }

    public function postHireCheckout(Request $request)
    {
        $items = Cart::instance('hire')->content();

//        dd(Cart::subtotal(2));

        $id = DB::table('mdr_orders')->insertGetId(
            [
                'order_number' =>  $this -> getOrderNumber(),
                'order_type' => "Hire",
                'name' => $request->input('name'),
                'address' => $request->input('address'),
                'email' => $request->input('email'),
                'phone' => $request->input('phone'),
                'details' => $request->input('details'),
                'start_date' => Carbon::parse($request->input('start_date')) ,
                'end_date' => Carbon::parse($request->input('end_date')) ,
                'total' =>(int)Cart::subtotal(),
                'status' => 1,
                'created_at' => Carbon::now()
            ]
        );


        // push order details
        foreach ($items as $item)
        {
            $order_details =  DB::table('mdr_order_details')->insert([
                [
                    'order_id' => $id,
                    'product_id' => $item -> id,
                    'quantity' => $item -> qty,
                    'price' => $item -> price
                ]
            ]);
        }

        Cart::instance('hire')->destroy();


        return back()->with('success','Created Hire Order Successfully :: You will receive an Email confirmation on order confirmation');
    }

    public function removeHire($id)
    {
        Cart::instance('hire')->remove($id);
        return redirect('cart/hire')->withSuccessMessage('Hire item has been removed!');
    }

    public function order(Request $request)
    {
        $contents = Cart::content();
        // order
        $id = DB::table('mdr_orders')->insertGetId(
            [
                'order_number' => $this -> getOrderNumber(),
                'order_type' => "Purchase",
                'name' => $request->input('name'),
                'address' => $request->input('address'),
                'email' => $request->input('email'),
                'phone' => $request->input('phone'),
                'details' => $request->input('details'),
                'total' => Cart::total(),
                'status' => 1,
                'created_at' => Carbon::now()
            ]
        );

        // push order details
        foreach ($contents as $content)
        {
            $order_details =  DB::table('mdr_order_details')->insert([
                [
                    'order_id' => $id,
                    'product_id' => $content -> id,
                    'quantity' => $content -> qty,
                    'price' => $content -> price
                ]
            ]);

            // notification
            DB::table('mdr_notifications')->insert([
                [
                    'title' => "Products Order",
                    'message' => "An order has been placed on",
                    'status' => 1,
                    'created_at' => Carbon::now()
                ]
            ]);

            if($order_details)
            {
                Cart::remove($content -> qty);
            }
        }

        // if the cart is empty .. destroy the cart
        if(empty(Cart::content()))
            Cart::remove();

        return redirect()->route('product')->with('message','Order has been made successfully');
    }


    public function getOrderNumber()
    {
        $characters = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789';

        $string = '';
        $max = strlen($characters) - 1;
        for ($i = 0; $i < 8; $i++) {
            $string .= $characters[mt_rand(0, $max)];
        }

        return $string;
    }


}
