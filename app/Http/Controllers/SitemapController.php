<?php

namespace App\Http\Controllers;

use App\Products;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\SitemapGenerator;
use Spatie\Tags\Url;

class SitemapController extends Controller
{

    public function index()
    {
        SitemapGenerator::create('http://www.medirsystems.co.ke')->writeToFile(public_path('sitemap.xml'));
    }

}
