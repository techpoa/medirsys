<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//
//    return view('index',compact('featured','items'));
//});

Route::get('/', 'PageController@index');
Route::get('about', 'PageController@about');
Route::get('about', 'PageController@about');
Route::get('product', 'PageController@product');
Route::post('product', 'PageController@filterProduct');
Route::get('services', 'PageController@services');
Route::get('product/details/{id}', 'PageController@details');
Route::get('product/details/{id}', 'PageController@details');
Route::get('product/hiredetails/{id}', 'PageController@hiredetails');
Route::post('product/brochure/{id}', 'PageController@downloadBrochure');
Route::get('hire', 'PageController@hire');
Route::post('hire', 'PageController@filterHire');
Route::get('contacts', 'PageController@contacts');
Route::post('contacts', 'PageController@postContacts');
Route::resource('sitemap', 'SitemapController');



Auth::routes();

Route::get('/admin', 'AdminController@index')->name('admin');
Route::get('/home', 'AdminController@index');
Route::get('/admin/profile', 'AdminController@profile')->name('adminprofile');
Route::get('admin/password', 'AdminController@password')->name('adminpassword');
Route::post('admin/password', 'AdminController@postPassword');
Route::post('admin/profile', 'AdminController@postProfile')->name('postprofile');

Route::get('activeusers', 'UserController@activeUsers');
Route::get('inactiveusers', 'UserController@inactiveUsers');
Route::post('users/activate', 'UserController@activate')->name('activateuser');
Route::post('users/deactivate', 'UserController@deactivate')->name('deactivateuser');
Route::resource('users', 'UserController');

Route::get('activeroles', 'RoleController@activeRoles');
Route::resource('roles','RoleController');

Route::get('activecategories', 'CategoryController@activeCategories');
Route::resource('categories','CategoryController');

Route::get('activeproducts', 'ProductController@activeProducts');
Route::post('products/activate', 'ProductController@activate')->name('activateproduct');
Route::post('products/deactivate', 'ProductController@deactivate')->name('deactivateproduct');
Route::post('products/yesforsale', 'ProductController@yesForSale')->name('yesforsale');
Route::post('products/noforsale', 'ProductController@noForSale')->name('noforsale');
Route::post('products/yesforhire', 'ProductController@yesForHire')->name('yesforhire');
Route::post('products/noforhire', 'ProductController@noForHire')->name('noforhire');
Route::post('products/yesforoffer', 'ProductController@yesForOffer')->name('yesforoffer');
Route::post('products/noforoffer', 'ProductController@noForOffer')->name('noforoffer');
Route::resource('products','ProductController');

Route::get('activeorders', 'OrderController@activeOrders');
Route::post('activeorderdetails', 'OrderController@activeOrderDetails');
Route::post('orders/accept', 'OrderController@accept')->name('acceptorder');
Route::post('orders/decline', 'OrderController@decline')->name('declineorder');
Route::get('orderdetailsdata','OrderController@orderDetailsData');
Route::resource('orders','OrderController');

Route::get('activesubscriptions', 'SubscriptionController@activeSubscriptions');
Route::resource('subscriptions','SubscriptionController');

Route::delete('emptyCart', 'CartController@emptyCart');
Route::get('cart/checkout', 'CartController@getCheckout');
Route::post('cart/checkout', 'CartController@postCheckout')->name('checkout');
Route::get('cart/hirecheckout', 'CartController@getHireCheckout');
Route::post('cart/posthirecheckout', 'CartController@postHireCheckout')->name('posthirecheckout');
Route::post('cart/hire', 'CartController@postHireCart');
Route::get('cart/hire', 'CartController@getHireCart');
Route::post('cart/removehire/{$id}', 'CartController@removeHire')->name('removehire');
Route::post('cart/order', 'CartController@order');
Route::resource('cart', 'CartController');

Route::resource('configurations', 'ConfigurationController');

Route::get('activenotifications', 'NotificationsController@activeNotifications');
Route::resource('notifications', 'NotificationsController');

